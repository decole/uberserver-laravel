FROM registry.gitlab.com/decole/uberserver-laravel:base

MAINTAINER decole <decole2014@yandex.ru>

ARG NEW_RELIC_AGENT_VERSION
ARG NEW_RELIC_LICENSE_KEY
ARG NEW_RELIC_SUPERVISORNAME
ARG NEW_RELIC_DAEMON_ADDRESS

# Download and install NewRelic PHP extension. This generates newrelic.ini file at /usr/local/etc/php/conf.d/newrelic.ini
RUN curl -L "https://download.newrelic.com/php_agent/archive/${NEW_RELIC_AGENT_VERSION}/newrelic-php5-${NEW_RELIC_AGENT_VERSION}-linux.tar.gz" | tar -C /tmp -zx \
 && export NR_INSTALL_USE_CP_NOT_LN=1 \
 && export NR_INSTALL_SILENT=1 \
 && /tmp/newrelic-php5-*/newrelic-install install \
 && rm -rf /tmp/newrelic-php5-* /tmp/nrinstall*

# Set parameter values in newrelic.ini file e.g. license key
#RUN sed -i -e s/\"REPLACE_WITH_REAL_KEY\"/${NEW_RELIC_LICENSE_KEY}/ \
# -e s/newrelic.appname[[:space:]]=[[:space:]].\*/newrelic.appname="${NEW_RELIC_SUPERVISORNAME}"/ \
# -e s/\;newrelic.daemon.address[[:space:]]=[[:space:]].\*/newrelic.daemon.address="${NEW_RELIC_DAEMON_ADDRESS}"/ \
#    /usr/local/etc/php/conf.d/newrelic.ini

# Install supervisor
RUN apt-get update \
 && apt-get install -y --no-install-recommends supervisor
COPY docker-compose/worker/supervisor/supervisord.conf /etc/supervisor

# Clear
RUN rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/* \
    && docker-php-source delete

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory
COPY . /var/www

# Set working directory
WORKDIR /var/www

# Configs, etc
COPY docker-compose/php/local.ini /usr/local/etc/php/conf.d/local.ini

RUN mkfifo /tmp/stdout && chmod 777 /tmp/stdout
