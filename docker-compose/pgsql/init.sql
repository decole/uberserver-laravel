-- Database: uberserver_smarthome_test

DROP DATABASE IF EXISTS uberserver_smarthome_test;

CREATE DATABASE uberserver_smarthome_test
    WITH
    OWNER = uberserver
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

GRANT CREATE, CONNECT ON DATABASE uberserver_smarthome_test TO uberserver;
GRANT TEMPORARY ON DATABASE uberserver_smarthome_test TO uberserver WITH GRANT OPTION;

GRANT TEMPORARY, CONNECT ON DATABASE uberserver_smarthome_test TO PUBLIC;
