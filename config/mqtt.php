<?php

return [
    'name' => 'Mqtt',
    'mqtt_broker_ip' => env('MQTT_BROKER','192.168.1.5'),
    'mqtt_broker_port' => env('MQTT_PORT', 1883),
];
