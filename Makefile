message = @echo "\n----------------------------------------\n$(1)\n----------------------------------------\n"

ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))

root = $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
compose = docker-compose

app = $(compose) exec -T app
artisan = $(app) php artisan

pull:
	$(compose) pull

up:
	$(compose) up -d --remove-orphans

stop:
	$(compose) stop

down:
	$(compose) down

restart: down up
	$(call message,"Restart completed")

update: down
	$(compose) pull
	$(MAKE) up
	$(call message,"Update completed")

app-install:
	up
	down
	cp -n .env.example .env
	up
	$(app) composer install
	perm
	$(app) php artisan key:generate
	$(app) php artisan migrate

console-in:
	$(compose) exec app bash

migrate:
	$(app) php artisan migrate --force

test:
	$(app) vendor/phpunit/phpunit/phpunit

ps:
	$(compose) ps

build:
	$(compose) build --no-cache app supervisor

perm:
	sudo chown -R ${USER}:${USER} vendor
	sudo chown -R ${USER}:${USER} tests
	sudo chown -R ${USER}:${USER} storage
	sudo chown -R ${USER}:${USER} database
