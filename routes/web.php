<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('/home');
    }

    return redirect('/login');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/watering', [App\Http\Controllers\HomeController::class, 'watering'])->name('main-watering');
Route::get('/fire-system', [App\Http\Controllers\HomeController::class, 'fireSystem'])->name('main-fire-system');
Route::get('/security-system', [App\Http\Controllers\HomeController::class, 'securitySystem'])->name('main-security-system');
Route::get('/all-data', [App\Http\Controllers\HomeController::class, 'allData'])->name('main-all-data');
Route::get('/margulis', [App\Http\Controllers\HomeController::class, 'margulis'])->name('main-margulis');
Route::get('/greenhouse', [App\Http\Controllers\HomeController::class, 'greenhouse'])->name('main-greenhouse');

Auth::routes();
