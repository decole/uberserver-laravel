<?php

namespace App\Dto;

class NotifyDto
{
    public string $topic;

    public string $message;

    // info / alert
    public string $type = 'alert';
}
