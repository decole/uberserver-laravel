<?php

namespace App\Listeners;

use App\Events\NewAlertNotify;
use App\Services\NotificationService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotify
{
    private NotificationService $service;

    public function __construct(NotificationService $service)
    {
        $this->service = $service;
    }

    public function handle(NewAlertNotify $event)
    {
        $notify = $event->notify;
        $this->service->send($notify);
    }
}
