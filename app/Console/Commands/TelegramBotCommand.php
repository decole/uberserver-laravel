<?php

namespace App\Console\Commands;

use App\Services\TelegramService;
use Illuminate\Console\Command;

class TelegramBotCommand extends Command
{
    protected $signature = 'telegram:bot';

    protected $description = 'Start telegram bot';

    private TelegramService $service;

    public function __construct(TelegramService $service)
    {
        $this->service = $service;

        parent::__construct();
    }

    public function handle(): void
    {
        while (true) {
            $this->service->BotRun();
            sleep(7);
        }
    }
}
