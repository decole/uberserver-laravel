<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \Modules\Mqtt\Console\MqttCommand::class,
            \Modules\AutoWatering\Console\WateringCommand::class,
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (explode(':', env('APP_URL'))[0] == 'https' || env('APP_ENV') == 'production') {
            \URL::forceScheme('https');
        }
    }
}
