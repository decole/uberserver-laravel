<?php

namespace App\Presenters\Interfaces;

interface PresenterInterface
{
    public function present(): array;
}
