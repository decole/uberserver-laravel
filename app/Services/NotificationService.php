<?php

namespace App\Services;

use App\Dto\NotifyDto;
use App\Services\Notification\DiscordNotify;
use App\Services\Notification\TelegramNotify;

class NotificationService
{
    public function send(NotifyDto $notify) {
        // Todo переделать на мапу
//        $map = [
//            'alert' => [TelegramNotify::class],
//            'info' => TelegramNotify::class,
//        ];
//
//        if (array_key_exists($notify->type, $map)) {
//            $service = $map[$notify->type];
//        }

        switch ($notify->type) {
            case 'alert':
                (new TelegramNotify())->send($notify);
//                (new DiscordNotify())->send($notify);

                break;
            case 'info':
                (new TelegramNotify())->send($notify);

                break;
            default:
                (new TelegramNotify())->send($notify);
        }
    }
}
