<?php

namespace App\Services\Notification;

use App\Dto\NotifyDto;
use App\Services\DiscordService;

class DiscordNotify implements NotifyInterface
{
    private DiscordService $service;

    public function __construct()
    {
        $this->service = new DiscordService();
    }

    public function send(NotifyDto $notify)
    {
        $this->service->send($notify);
    }
}
