<?php

namespace App\Services\Notification;

use App\Dto\NotifyDto;

interface NotifyInterface
{
    public function send(NotifyDto $notify);
}
