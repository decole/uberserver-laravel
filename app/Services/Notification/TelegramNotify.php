<?php

namespace App\Services\Notification;

use App\Dto\NotifyDto;
use App\Services\TelegramService;

class TelegramNotify implements NotifyInterface
{
    private TelegramService $service;

    public function __construct()
    {
        $this->service = new TelegramService();
    }

    public function send(NotifyDto $notify)
    {
        $message = $this->prepareMessage($notify);
        $this->service->sendMessage($message);
    }

    private function prepareMessage(NotifyDto $notify): string
    {
        if ($notify->type == 'alert') {
            return "
*Внимание!*

```
topic: {$notify->topic}
message: {$notify->message}
```";
        }

        return "
topic: {$notify->topic}
> {$notify->message}";
    }
}
