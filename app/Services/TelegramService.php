<?php

namespace App\Services;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class TelegramService
{
    private string $bot_api_key;

    private string $bot_username;

    public function __construct()
    {
        $this->bot_api_key  = env('PHP_TELEGRAM_BOT_API_KEY', '');
        $this->bot_username = env('PHP_TELEGRAM_BOT_NAME', '');
    }

    public function BotRun()
    {
        try {
            $telegram = new Telegram($this->bot_api_key, $this->bot_username);
            $telegram->addCommandsPath('/var/www/app/Console/TelegramCommands');
            $telegram->setDownloadPath('/var/www/storage/app/public');
            $telegram->useGetUpdatesWithoutDatabase();
            $server_response = $telegram->handleGetUpdates();

            if (!$server_response->isOk()) {
                echo date('Y-m-d H:i:s') . ' - Failed to fetch updates' . PHP_EOL;
                echo $server_response->printError();
            }
        } catch (TelegramException $e) {
             echo $e->getMessage();
        }
    }

    public function sendMessage(string $message, $userId = null)
    {
        if (!$userId) {
            $userId = env('PHP_TELEGRAM_ADMIN_USER_ID', '');
        }

        Request::sendMessage([
            'chat_id' => $userId,
            'text'    => $message,
            'parse_mode' => 'markdown',
        ]);
    }
}
