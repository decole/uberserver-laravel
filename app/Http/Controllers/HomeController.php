<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function watering()
    {
        return view('watering');
    }

    public function fireSystem()
    {
        return view('fire-system');
    }

    public function securitySystem()
    {
        return view('security-system');
    }

    public function allData()
    {
        return view('all-data');
    }

    public function margulis()
    {
        return view('margulis');
    }

    public function greenhouse()
    {
        return view('greenhouse');
    }
}
