<?php

namespace Modules\Sensor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSensorRequest extends FormRequest
{
//    protected $stopOnFirstFailure = true;

    public function rules(): array
    {
        return [
            'name' => 'required|unique:sensors|max:300',
            'topic' => 'required|max:400',
            'payload' => 'required',
            'payload_min' => 'required',
            'payload_max' => 'required',
            'message_info' => 'required|max:255',
            'message_ok' => 'required|max:255',
            'message_warn' => 'required|max:255',
            'type'  => 'required|integer|between:0,2', // see Sensor::TYPES
            'status' => 'nullable',
            'notify' => 'nullable',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
