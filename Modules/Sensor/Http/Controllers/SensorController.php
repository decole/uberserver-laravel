<?php

namespace Modules\Sensor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Sensor\Http\Requests\CreateSensorRequest;
use Modules\Sensor\Http\Requests\UpdateSensorRequest;
use Modules\Sensor\Services\SensorService;

class SensorController extends Controller
{
    private SensorService $service;

    public function __construct(SensorService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $sensors = $this->service->getAll();

        return view('sensor::index', compact('sensors'));
    }

    public function create()
    {
        return view('sensor::create');
    }

    public function store(CreateSensorRequest $request)
    {
        $validated = $request->validated();
        $this->service->create($validated);

        return redirect()->route('sensor.index')->with('success','Sensor created successfully.');
    }

    /**
     * not used
     */
    public function show($id)
    {
        return view('sensor::show');
    }

    public function edit($id)
    {
        $sensor = $this->service->getOne($id);

        return view('sensor::edit', compact('sensor'));
    }

    public function update(UpdateSensorRequest $request, $id)
    {
        $validated = $request->validated();
        $update = $this->service->update($id, $validated);

        if (!$update) {
            return redirect()->route('sensor.index')
                ->with('error', 'Sensor not updated');
        }

        return redirect()->route('sensor.index')
            ->with('success','Sensor ' . $id . ' updated successfully');
    }

    public function destroy($id)
    {
        $delete = $this->service->delete($id);

        if (!$delete) {
            return redirect()->route('sensor.index')
                ->with('error', 'Sensor not found');
        }

        return redirect()->route('sensor.index')
            ->with('success','Sensor deleted successfully');
    }
}
