<?php

Route::prefix('sensor')->group(function() {
    Route::get('/', 'SensorController@index')->name('sensor.index')->middleware('auth');
    Route::get('/create', 'SensorController@create')->name('sensor.create')->middleware('auth');
    Route::post('/store', 'SensorController@store')->name('sensor.store')->middleware('auth');
    Route::get('/show/{id}', 'SensorController@show')->name('sensor.show')->where('id', '[0-9]+')->middleware('auth');
    Route::get('/edit/{id}', 'SensorController@edit')->name('sensor.edit')->where('id', '[0-9]+')->middleware('auth');
    Route::post('/update/{id}', 'SensorController@update')->name('sensor.update')->where('id', '[0-9]+')->middleware('auth');
    Route::get('/destroy/{id}', 'SensorController@destroy')->name('sensor.destroy')->where('id', '[0-9]+')->middleware('auth');
});
