<?php

namespace Modules\Sensor\Database\Repositories;

use Modules\Sensor\Entities\Sensor;

class SensorRepository
{
    public function getOne(int $id): ?Sensor
    {
        return Sensor::where('id', $id)->first();
    }

    public function getAll()
    {
        return Sensor::orderBy('id', 'ASC')->get()->all();
    }

    public function create(array $fields): void
    {
        Sensor::create($fields);
    }

    public function update(int $id, array $fields): bool
    {
        $sensor = $this->getOne($id);

        if (!$sensor) {
            return false;
        }

        return $sensor->update($fields);
    }

    public function delete(array $ids): ?bool
    {
        return Sensor::whereIn('id', $ids)->delete();
    }
}
