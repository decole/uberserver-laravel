<?php

namespace Modules\Sensor\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SensorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = [
            [
                'name' => 'Пристройка - температура',
                'topic' => 'margulis/temperature',
                'payload' => '',
                'payload_min' => '12',
                'payload_max' => '35',
                'message_info' => 'температура в пристройке',
                'message_ok' => 'температура в пристройке - {value} °C',
                'message_warn' => 'Температура в пристройке - {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Пристройка - влажность',
                'topic' => 'margulis/humidity',
                'payload' => '',
                'payload_min' => '20',
                'payload_max' => '90',
                'message_info' => 'сенсор влажности в пристройке DHT11',
                'message_ok' => 'Влажность в пристройе {value} %',
                'message_warn' => 'Критическое влажность в пристройе {value} %',
                'type' => '1',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Холодная прихожка - температура',
                'topic' => 'holl/temperature',
                'payload' => '',
                'payload_min' => '2',
                'payload_max' => '40',
                'message_info' => 'температура в холодной прихожке',
                'message_ok' => 'температура в холодной прихожке - {value} °C',
                'message_warn' => 'Внимание! температура в холодной прихожке - {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Низа - температура',
                'topic' => 'underflor/temperature',
                'payload' => '',
                'payload_min' => '6',
                'payload_max' => '28',
                'message_info' => 'температура в низах',
                'message_ok' => 'температура в низах - {value} °C',
                'message_warn' => 'Внимание! температура в низах - {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Туалет - температура',
                'topic' => 'home/restroom/temperature',
                'payload' => '',
                'payload_min' => '14',
                'payload_max' => '35',
                'message_info' => 'температура в туалете',
                'message_ok' => 'температура в туалете - {value} °C',
                'message_warn' => 'Внимание! температура в туалете - {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Кухня - температура',
                'topic' => 'home/kitchen/temperature',
                'payload' => '',
                'payload_min' => '14',
                'payload_max' => '45',
                'message_info' => 'температура на кухне',
                'message_ok' => 'температура на кухне - {value} °C',
                'message_warn' => 'Внимание! температура на кухне - {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Зал - температура',
                'topic' => 'home/hall/temperature',
                'payload' => '',
                'payload_min' => '19',
                'payload_max' => '37',
                'message_info' => 'температура в зале',
                'message_ok' => 'температура в зале - {value} °C',
                'message_warn' => 'Внимание! температура в зале - {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Холодная прихожка - влажность',
                'topic' => 'holl/humidity',
                'payload' => '',
                'payload_min' => '12',
                'payload_max' => '90',
                'message_info' => 'сенсор влажности в холодная прихожке DHT11',
                'message_ok' => 'Влажность в холодная прихожке {value} %',
                'message_warn' => 'Внимание! Влажность в холодная прихожке {value} %',
                'type' => '1',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Теплица - температура',
                'topic' => 'greenhouse/temperature',
                'payload' => '',
                'payload_min' => '-25',
                'payload_max' => '60',
                'message_info' => 'температура в теплице',
                'message_ok' => 'температура в теплице - {value} °C',
                'message_warn' => 'Предельная температура в теплице {value} °C',
                'type' => '0',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Датчик протечки в низах',
                'topic' => 'water/leakage',
                'payload' => '',
                'payload_min' => '0',
                'payload_max' => '0',
                'message_info' => 'Датчик протечки в низах',
                'message_ok' => 'Датчик протечки, состояние: {value}',
                'message_warn' => 'Протечка в низах! состояние: {value}',
                'type' => '2',
                'status' => '1',
                'notify' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ];

        foreach ($values as $value) {
            DB::table('sensors')->where('topic', '=', $value['topic'])->delete();
            DB::table('sensors')->insert($value);
        }
    }
}
