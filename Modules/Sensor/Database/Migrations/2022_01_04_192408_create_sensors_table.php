<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensors', function (Blueprint $table) {
            $table->id();
            $table->string('name', '300')->default('');
            $table->string('topic', '400')->unique();
            $table->string('payload')->default('');
            $table->string('message_info')->default('');
            $table->string('message_ok')->default('');
            $table->string('message_warn')->default('');
            $table->integer('type')->default(0);
            $table->tinyInteger('status', false, true)->default(0);
            $table->tinyInteger('notify', false, true)->default(0);
            $table->timestamps();

            $table->index(['topic']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensors');
    }
}
