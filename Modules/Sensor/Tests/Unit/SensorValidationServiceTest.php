<?php

namespace Modules\Sensor\Tests\Unit;

use Modules\Sensor\Services\ServiceEntity\HumiditySensorValidation;
use Modules\Sensor\Services\ServiceEntity\LeakageSensorValidation;
use Modules\Sensor\Services\ServiceEntity\TemperatureSensorValidation;
use Mosquitto\Message;
use App\Events\NewAlertNotify;
use Modules\AutoWatering\Events\MqttMessagePosting;
use Modules\Sensor\Entities\Sensor;
use Modules\Sensor\Services\ValidationService;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class SensorValidationServiceTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    protected ValidationService $service;
    protected string $topicTemperature = 'test-temperature-sensor';
    protected string $topicHumidity = 'test-humidity-sensor';
    protected string $topicLeakage = 'test-leakage-sensor';

    public function  setUp(): void
    {
        parent::setUp();

        $this->service = new ValidationService();
        $this->prepareTestSensors();
    }

    public function testValidationServiceTemperatureSensor()
    {
        Event::fake();
        $sensor = Sensor::where('topic', $this->topicTemperature)->first();
        $message = $this->createMessage($sensor->topic, $sensor->payload_min);
        $this->service->validateMessage($message);

        $this->assertIsObject($this->service->getSensors());
        Event::assertNotDispatched(MqttMessagePosting::class);

        $validator = new TemperatureSensorValidation();

        $state = $validator->validate($sensor, $message);
        $this->assertEquals(true, $state);

        $message->payload = 80;
        $state = $validator->validate($sensor, $message);
        $this->assertEquals(false, $state);

        $this->service->validateMessage($message);
        Event::assertDispatched(NewAlertNotify::class);
        Event::assertDispatchedTimes(NewAlertNotify::class, 1);
    }

    public function testValidationServiceHumiditySensor()
    {
        Event::fake();
        $sensor = Sensor::where('topic', $this->topicHumidity)->first();
        $message = $this->createMessage($sensor->topic, $sensor->payload_min);
        $this->service->validateMessage($message);

        $this->assertIsObject($this->service->getSensors());
        Event::assertNotDispatched(MqttMessagePosting::class);

        $validator = new HumiditySensorValidation();

        $state = $validator->validate($sensor, $message);
        $this->assertEquals(true, $state);

        $message->payload = 20;
        $state = $validator->validate($sensor, $message);
        $this->assertEquals(false, $state);

        $this->service->validateMessage($message);
        Event::assertDispatched(NewAlertNotify::class);
        Event::assertDispatchedTimes(NewAlertNotify::class, 1);
    }

    public function testValidationServiceLeakageSensor()
    {
        Event::fake();
        $sensor = Sensor::where('topic', $this->topicLeakage)->first();
        $message = $this->createMessage($sensor->topic, $sensor->payload_min);
        $this->service->validateMessage($message);

        $this->assertIsObject($this->service->getSensors());
        Event::assertNotDispatched(MqttMessagePosting::class);

        $validator = new LeakageSensorValidation();

        $state = $validator->validate($sensor, $message);
        $this->assertEquals(true, $state);

        $message->payload = 1;
        $state = $validator->validate($sensor, $message);
        $this->assertEquals(false, $state);

        $this->service->validateMessage($message);
        Event::assertDispatched(NewAlertNotify::class);
        Event::assertDispatchedTimes(NewAlertNotify::class, 1);
    }

    protected function createMessage(string $topic, string $payload):  Message
    {
        $message = new Message();
        $message->topic = $topic;
        $message->payload = $payload;

        return $message;
    }

    protected function prepareTestSensors()
    {
        // temperature sensor
        $sensor = Sensor::where('topic', $this->topicTemperature)->first();

        if (!$sensor) {
            $sensor = new Sensor();
            $sensor->name = 'temperature sensor';
            $sensor->topic = $this->topicTemperature;
            $sensor->payload_min = 10;
            $sensor->payload_max = 25;
            $sensor->message_info = 'message_info';
            $sensor->message_ok = 'message_ok';
            $sensor->message_warn = 'message_warn';
            $sensor->type = 0; //Sensor::TYPES;
            $sensor->status = 1;
            $sensor->notify = 1;
            $sensor->save();
        }

        // humidity sensor
        $sensor = Sensor::where('topic', $this->topicHumidity)->first();

        if (!$sensor) {
            $sensor = new Sensor();
            $sensor->name = 'humidity sensor';
            $sensor->topic = $this->topicHumidity;
            $sensor->payload_min = 40;
            $sensor->payload_max = 80;
            $sensor->message_info = 'message_info';
            $sensor->message_ok = 'message_ok';
            $sensor->message_warn = 'message_warn';
            $sensor->type = 2; //Sensor::TYPES;
            $sensor->status = 1;
            $sensor->notify = 1;
            $sensor->save();
        }

        // leakage sensor
        $sensor = Sensor::where('topic', $this->topicLeakage)->first();

        if (!$sensor) {
            $sensor = new Sensor();
            $sensor->name = 'leakage sensor';
            $sensor->topic = $this->topicLeakage;
            $sensor->payload_min = 0;
            $sensor->payload_max = 0;
            $sensor->message_info = 'message_info';
            $sensor->message_ok = 'message_ok';
            $sensor->message_warn = 'message_warn';
            $sensor->type = 2; //Sensor::TYPES;
            $sensor->status = 1;
            $sensor->notify = 1;
            $sensor->save();
        }
    }
}
