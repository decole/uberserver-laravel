<?php

namespace Modules\Sensor\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Mqtt\Events\MqttMessagePublished;
use Modules\Sensor\Listeners\NewMqttMessage;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        MqttMessagePublished::class => [
            NewMqttMessage::class,
        ],
    ];
}
