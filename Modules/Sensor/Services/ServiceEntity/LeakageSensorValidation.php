<?php

namespace Modules\Sensor\Services\ServiceEntity;

use Modules\Sensor\Entities\Sensor;
use Mosquitto\Message;

class LeakageSensorValidation implements SensorTypeValidationInterface
{
    /**
     * valid - true
     * not valid - false
     */
    public function validate(Sensor $sensor, Message $message): bool
    {
        $payload = $message->payload;

        // payload_max and payload_min is = 0 always, if status is not leakage
        if ($sensor->payload_max == 0) {
            return ((int)$payload == $sensor->payload_max);
        }

        // if payload is string, example: "ok"
        return ($payload == $sensor->payload_max || $payload == $sensor->payload_min);
    }
}
