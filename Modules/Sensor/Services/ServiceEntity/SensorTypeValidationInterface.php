<?php

namespace Modules\Sensor\Services\ServiceEntity;

use Modules\Sensor\Entities\Sensor;
use Mosquitto\Message;

interface SensorTypeValidationInterface
{
    public function validate(Sensor $sensor, Message $message): bool;
}
