<?php

namespace Modules\Sensor\Services\ServiceEntity;

use Modules\Sensor\Entities\Sensor;
use Mosquitto\Message;

class TemperatureSensorValidation implements SensorTypeValidationInterface
{
    /**
     * valid - true
     * not valid - false
     */
    public function validate(Sensor $sensor, Message $message): bool
    {
        $payload = $message->payload;

        return !($payload > $sensor->payload_max || $payload < $sensor->payload_min);
    }
}
