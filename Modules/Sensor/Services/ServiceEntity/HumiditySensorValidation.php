<?php

namespace Modules\Sensor\Services\ServiceEntity;

use Modules\Sensor\Entities\Sensor;
use Mosquitto\Message;

class HumiditySensorValidation implements SensorTypeValidationInterface
{
    /**
     * valid - true
     * not valid - false
     */
    public function validate(Sensor $sensor, Message $message): bool
    {
        $payload = (int)$message->payload;

        return !($payload > (int)$sensor->payload_max || $payload < (int)$sensor->payload_min);
    }
}
