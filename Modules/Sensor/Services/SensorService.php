<?php

namespace Modules\Sensor\Services;

use Modules\Sensor\Database\Repositories\SensorRepository;
use Illuminate\Support\Facades\Cache;
use Modules\Sensor\Entities\Sensor;

class SensorService
{
    private SensorRepository $repository;

    public function __construct(SensorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getOne($id): ?Sensor
    {
        return $this->repository->getOne($id);
    }

    /**
     * @return Sensor[]|mixed
     */
    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function create(array $fields): void
    {
        $fields['status'] = isset($fields['status']) ? 1 : 0;
        $fields['notify'] = isset($fields['notify']) ? 1 : 0;

        $this->repository->create($fields);
        $this->forgetCache();
    }

    public function update(int $id, array $fields): bool
    {
        $fields['status'] = isset($fields['status']) ? 1 : 0;
        $fields['notify'] = isset($fields['notify']) ? 1 : 0;

        $status = $this->repository->update($id, $fields);
        $this->forgetCache();

        return $status;
    }

    public function delete(int $id): bool
    {
        $status = $this->repository->delete([$id]);
        $this->forgetCache();

        return $status;
    }

    private function forgetCache()
    {
        Cache::forget(ValidationService::SENSOR_CACHE_KEY);
        Cache::forget(ValidationService::SENSOR_TOPICS_CACHE_KEY);
    }
}
