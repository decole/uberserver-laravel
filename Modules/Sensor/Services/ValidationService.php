<?php

namespace Modules\Sensor\Services;

use App\Dto\NotifyDto;
use App\Events\NewAlertNotify;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Modules\Sensor\Entities\Sensor;
use Modules\Sensor\Services\ServiceEntity\HumiditySensorValidation;
use Modules\Sensor\Services\ServiceEntity\LeakageSensorValidation;
use Modules\Sensor\Services\ServiceEntity\SensorTypeValidationInterface;
use Modules\Sensor\Services\ServiceEntity\TemperatureSensorValidation;
use Mosquitto\Message;

class ValidationService
{
    public const CACHE_TIME = 9000;
    public const SENSOR_CACHE_KEY = 'sensors';
    public const SENSOR_TOPICS_CACHE_KEY = 'sensor-topics';
    private const MAP =  [
        Sensor::TEMPERATURE_SENSOR => TemperatureSensorValidation::class,
        Sensor::HUMIDITY_SENSOR => HumiditySensorValidation::class,
        Sensor::LEAKAGE_SENSOR => LeakageSensorValidation::class,
    ];

    public function validateMessage(Message $message): void
    {
        if ($this->isSensor($message->topic)) {
            $sensors = $this->getSensors();
            $payload = $message->payload;

            // where mqtt client MQTT Explorer delete topic, it pushes null payload on topic current
            if ($payload == 'null' || $payload == '') {
                return;
            }

            /** @var Sensor[] $sensors */
            foreach ($sensors as $sensor) {
                if ($message->topic == $sensor->topic && $sensor->status == 1) {
                    if (!$this->validateSensor($sensor, $message) && $sensor->notify == 1) {
                        $this->sendNotify($sensor->message_warn . ' ' . $payload);
                    }
                }
            }
        }
    }

    private function sendNotify(string $message): void
    {
        $dto = new NotifyDto();
        $dto->topic = 'sensor';
        $dto->message = $message;

        NewAlertNotify::dispatch($dto);
    }

    public function getSensors(): Collection
    {
        return Cache::remember(self::SENSOR_CACHE_KEY, static::CACHE_TIME, function () {
            return Sensor::all();
        });
    }

    public function isSensor(string $topic): bool
    {
        $topics = Cache::remember(self::SENSOR_TOPICS_CACHE_KEY, static::CACHE_TIME, function () {
            $sensors = $this->getSensors();
            $topics = [];

            foreach ($sensors as $sensor) {
                $topics[] = $sensor->topic;
            }

            return $topics;
        });

        return in_array($topic, $topics);
    }

    /**
     * valid - true
     * not valid - false
     */
    private function validateSensor(Sensor $sensor, Message $message): bool
    {
        $type = Sensor::TYPES[$sensor->type] ?? null;

        if ($type) {
            $map = static::MAP;
            /** @var SensorTypeValidationInterface $validator */
            $validator = new $map[$type];

            return $validator->validate($sensor, $message);
        }

        return false;
    }
}
