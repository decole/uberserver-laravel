<div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="fas fa-microchip"></i></span>
    <div class="info-box-content sensor-control" data-sensor-topic="{{ $topic }}" data-sensor-id="{{ $id }}">
        <span class="info-box-text">{{ $title }}</span>
        <span class="info-box-number">
            <span data-sensor-value="{{ $topic }}">{{ $payload}}</span>
        </span>
    </div>
</div>
