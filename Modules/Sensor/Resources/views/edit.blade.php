@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Изменение сенсора {{ $sensor->name }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">General Form</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit sensor {{ $sensor->name }} [{{ $sensor->id }}]</h3>
        </div>

        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('sensor.update', $sensor->id) }}">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Название сенсора</label>
                    <input type="text" name="name" value="{{ $sensor->name }}" class="form-control" placeholder="Кухня температура">
                </div>
                <div class="form-group">
                    <label>Топик</label>
                    <input type="text" name="topic" value="{{ $sensor->topic }}" class="form-control" placeholder="hall/example">
                </div>
                <div class="form-group">
                    <label>Значение минимальное. Для сенсоров протечки = 0</label>
                    <input type="text" name="payload_min" value="{{ $sensor->payload_min }}" class="form-control" placeholder="hall/example">
                </div>
                <div class="form-group">
                    <label>Значение максимальное. Для сенсоров протечки = 0</label>
                    <input type="text" name="payload_max" value="{{ $sensor->payload_max }}" class="form-control" placeholder="hall/example">
                </div>
                <div class="form-group">
                    <label>Текущее состояние сенсора</label>
                    <input type="text" name="payload" value="{{ $sensor->payload }}" class="form-control" placeholder="10">
                </div>
                <div class="form-group">
                    <label>Текст для информации</label>
                    <input type="text" name="message_info" value="{{ $sensor->message_info }}" class="form-control" placeholder="Сенсор Кухня температура">
                </div>
                <div class="form-group">
                    <label>Текст для статуса Oк</label>
                    <input type="text" name="message_ok" value="{{ $sensor->message_ok }}" class="form-control" placeholder="Сенсор Кухня температура, все хорошо">
                </div>
                <div class="form-group">
                    <label>Текст для вывода алерта</label>
                    <input type="text" name="message_warn" value="{{ $sensor->message_warn }}" class="form-control" placeholder="Внимание, что то произошло">
                </div>
                <div class="form-group">
                    <label>Тип сенсора</label>
                    <select class="form-control select2" name="type" style="width: 100%;">
                        @foreach (\Modules\Sensor\Entities\Sensor::TYPES as $key => $type)
                            <option value="{{ $key }}" @if($key === $sensor->type) selected="selected" @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="status" @if($sensor->status) checked @endif id="statusSwitch">
                    <label class="custom-control-label" for="statusSwitch">Статус сенсора</label>
                </div>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="notify" @if($sensor->notify) checked @endif id="notifySwitch">
                    <label class="custom-control-label" for="notifySwitch">Присылать оповещения</label>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
@endsection

@section('third_party_scripts')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
@endsection
