<?php

namespace Modules\Sensor\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Sensor
 *
 * @mixin Builder
 * @property int $id
 * @property string $name
 * @property string $topic
 * @property string $payload
 * @property string $payload_min
 * @property string $payload_max
 * @property string $message_info
 * @property string $message_ok
 * @property string $message_warn
 * @property int $type
 * @property int $status
 * @property int $notify
 * @property int $created_at
 * @property int $updated_at
 */
class Sensor extends Model
{
    use HasFactory;

    public const TEMPERATURE_SENSOR = 'temperature';
    public const HUMIDITY_SENSOR = 'humidity';
    public const LEAKAGE_SENSOR = 'leakage';

    public const TYPES = [
        0 => self::TEMPERATURE_SENSOR,
        1 => self::HUMIDITY_SENSOR,
        2 => self::LEAKAGE_SENSOR,
    ];

    protected $fillable = [
        'name',
        'topic',
        'payload',
        'payload_min',
        'payload_max',
        'message_info',
        'message_ok',
        'message_warn',
        'type',
        'status',
        'notify',
    ];

    protected static function newFactory()
    {
        return \Modules\Sensor\Database\factories\SensorFactory::new();
    }
}
