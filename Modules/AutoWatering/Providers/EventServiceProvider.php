<?php

namespace Modules\AutoWatering\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\AutoWatering\Listeners\MqttMessageGettingSubscriber;
use Modules\Mqtt\Events\MqttMessagePublished;
use Modules\AutoWatering\Listeners\NewMqttMessage;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        MqttMessagePublished::class => [
            NewMqttMessage::class,
        ],
    ];
}
