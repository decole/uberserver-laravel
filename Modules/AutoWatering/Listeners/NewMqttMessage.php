<?php

namespace Modules\AutoWatering\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Mqtt\Events\MqttMessagePublished;
use Modules\AutoWatering\Services\ValidationService;

class NewMqttMessage
{
    private ValidationService $service;

    public function __construct(ValidationService $service)
    {
        $this->service = $service;
    }

    public function handle(MqttMessagePublished $event)
    {
        $this->service->validateMessage($event->message);
    }
}
