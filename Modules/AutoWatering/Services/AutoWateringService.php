<?php

namespace Modules\AutoWatering\Services;

use App\Dto\NotifyDto;
use App\Events\NewAlertNotify;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Modules\AutoWatering\Database\Repositories\AutoWateringRepository;
use Modules\AutoWatering\Entities\AutoWateringScenarioSwitchState;
use Modules\AutoWatering\Entities\AutoWateringSchedule;
use Modules\AutoWatering\Events\MqttMessagePosting;
use Modules\Relay\Entities\Relay;

class AutoWateringService
{
    public const NOTIFY_CACHE_KEY = 'auto_watering_notify_key';

    public const NOTIFY_DELAY_SECONDS = 300;

    public const NOTIFY_TOPIC = 'autoWatering';

    private AutoWateringRepository $repository;

    public function __construct(AutoWateringRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return AutoWateringSchedule[]
     */
    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function getOne(int $id): ?AutoWateringSchedule
    {
        return $this->repository->getOne($id);
    }

    public function create(array $validated): void
    {
        $date = date('Y-m-d H:i:s');
        $validated['date_last_normal_on'] = $date;
        $validated['date_last_normal_off'] = $date;

        $this->repository->create($validated);
    }

    public function update(int $id, array $fields): bool
    {
        $fields['next_start'] = trim($fields['next_start']);
        $status = $this->repository->update($id, $fields);
        $this->forgetCache();

        return $status;
    }

    public function delete(int $id): ?bool
    {
        $status = $this->repository->delete([$id]);
        $this->forgetCache();

        return $status;
    }

    public function alarmStopAll(): void
    {
        $message = 'Зарегистрировано событие аварийного отключения всех клапанов автополива';
        $scenarios = $this->getAll();

        /** @var AutoWateringSchedule[] $scenarios */
        foreach ($scenarios as $scenario) {
            $this->repository->changeScenarioState($scenario->id, AutoWateringSchedule::STATE_EMERGENCY_SHUTDOWN);
        }

        $this->forgetCache();
        $swifts = $this->repository->getAllSwifts();

        foreach ($swifts as $swift) {
            $this->postTopicPayload($swift->topic, $swift->command_off);
            sleep(0.2);
        }

        Log::error($message);
        $this->sendNotify($message);
    }

    /**
     * Одинаковые сообщения нельзя отправлять self::NOTIFY_DELAY_SECONDS секунд
     * @param string $message
     * @return void
     */
    public function sendNotify(string $message): void
    {
        $key = self::NOTIFY_CACHE_KEY;

        $cached = Cache::get($key, function () use ($key, $message) {
            Cache::put($key, $message, self::NOTIFY_DELAY_SECONDS);
            $this->sendMessage($message);
        });

        if ($message !== $cached) {
            Cache::forget($key);
            $this->sendMessage($message);
        }
    }

    public function postTopicPayload(string $topic, string $payload): void
    {
        MqttMessagePosting::dispatch($topic, $payload);
    }

    public function changeScenarioState(int $scenarioId, int $statement): void
    {
        $this->repository->changeScenarioState($scenarioId, $statement);
    }

    public function changeScenarioNextStartDate(int $scenarioId, string $nextDate): void
    {
        $this->repository->changeScenarioNextStartDate($scenarioId, $nextDate);
    }

    public function ExecuteCommandSwifts(int $scenarioId): void
    {
        // relays with his states on table auto_watering_scenario_switch_state
        $relays = App::call(
            function (AutoWateringScenarioSwitchStateService $service) use ($scenarioId) {
                return $service->getScenarioSwifts($scenarioId);
            }
        );

        /** @var Relay $relay */
        foreach ($relays as $relay) {
            $command = $relay->state == AutoWateringScenarioSwitchState::ACTIVATE ? $relay->command_on : $relay->command_off;
            $this->postTopicPayload($relay->topic, $command);
            sleep(0.2);
        }
    }

    public function forgetCache(): void
    {
        Cache::forget(ValidationService::SWIFTS_CACHE_KEY);
        Cache::forget(ValidationService::SWIFT_TOPICS_CACHE_KEY);
        Cache::forget(ValidationService::SWIFTS_CHECK_TOPICS_CACHE_KEY);
        Cache::forget(ValidationService::SWIFT_COLLECTION_TOPICS_CACHE_KEY);
    }

    private function sendMessage(string $message): void
    {
        $dto = new NotifyDto();
        $dto->topic = self::NOTIFY_TOPIC;
        $dto->message = $message;

        NewAlertNotify::dispatch($dto);
    }
}
