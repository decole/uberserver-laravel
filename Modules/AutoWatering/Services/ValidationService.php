<?php

namespace Modules\AutoWatering\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Modules\AutoWatering\Database\Repositories\AutoWateringScenarioSwitchStateRepository;
use Modules\AutoWatering\Entities\AutoWateringSchedule;
use Modules\AutoWatering\Events\AllWateringSwiftsStoping;
use Modules\Relay\Database\Repositories\RelayRepository;
use Modules\Relay\Entities\Relay;
use Mosquitto\Message;

class ValidationService
{
    public const STATE_NORMAL = 0;
    public const STATE_EMERGENCY = 1;

    public const CACHE_TIME = 43200; // 30 days
    public const SWIFTS_CACHE_KEY = 'watering_swifts';
    public const SWIFT_TOPICS_CACHE_KEY = 'watering_swift_topics';
    public const SWIFTS_CHECK_TOPICS_CACHE_KEY = 'watering_swift_check_topics';
    public const SWIFT_COLLECTION_TOPICS_CACHE_KEY = 'watering_swift_collection_topics';

    public function validateMessage(Message $message): ?bool
    {
        if ($this->isWateringSwiftScenarioTopic($message->topic)) {
            $scenarioState = $this->checkScenarioState($message);

            if ($scenarioState === self::STATE_EMERGENCY) {
                $this->alarmStopAll();

                return false;
            }

            return true;
        }

        return null;
    }

    public function isNormalState(Message $message, AutoWateringSchedule $scenario): bool
    {
        return (new AutoWateringScenarioSwitchStateRepository())->isSwitchState($scenario->id, $message);
    }

    /**
     * @return AutoWateringSchedule[]
     */
    public function getScenarios(): array
    {
        return App::call(
            function (AutoWateringService $service) {
                return $service->getAll();
            }
        );
    }

    private function checkScenarioState(Message $message): int
    {
        $scenarios = $this->getScenarios();

        foreach ($scenarios as $scenario) {
            $status = (int)$scenario->status;

            if ($status === AutoWateringSchedule::STATE_EMERGENCY_SHUTDOWN) {
                $this->alarmStopAll();

                return self::STATE_EMERGENCY;
            }
            if ($status === AutoWateringSchedule::STATE_NORMALLY_ON && !$this->isNormalState($message, $scenario)) {
                // scenario switching transition process, you do not need to react to it - this is the first minute of the scenario
                if ($scenario->date_next_start == date('Y-m-d H:i:s')) {
                    continue;
                }

                return self::STATE_EMERGENCY;
            }
        }

        return self::STATE_NORMAL;
    }

    private function isWateringSwiftScenarioTopic(string $topic): bool
    {
        $topics = Cache::remember(self::SWIFT_COLLECTION_TOPICS_CACHE_KEY, static::CACHE_TIME, function () {
            return $this->getCheckSwiftTopics();
        });

        return in_array($topic, $topics);
    }

    private function getSwiftTopics(): array
    {
        return Cache::remember(self::SWIFT_TOPICS_CACHE_KEY, static::CACHE_TIME, function () {
            $swifts = $this->getAllWateringSwifts();
            $topics = [];

            /** @var Relay[] $swifts */
            foreach ($swifts as $swift) {
                $topics[] = $swift->topic;
            }

            return $topics;
        });
    }

    private function getCheckSwiftTopics(): array
    {
        return Cache::remember(self::SWIFTS_CHECK_TOPICS_CACHE_KEY, static::CACHE_TIME, function () {
            $swifts = $this->getAllWateringSwifts();
            $topics = [];

            /** @var Relay[] $swifts */
            foreach ($swifts as $swift) {
                $topics[] = $swift->check_topic;
            }

            return $topics;
        });
    }

    private function getAllWateringSwifts(): array
    {
        return Cache::remember(self::SWIFTS_CACHE_KEY, static::CACHE_TIME, function () {
            return (new RelayRepository())->getAllByType(Relay::TYPE_SWIFT);
        });
    }

    private function alarmStopAll(): void
    {
        AllWateringSwiftsStoping::dispatch();
    }
}
