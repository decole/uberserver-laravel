<?php

namespace Modules\AutoWatering\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Modules\AutoWatering\Database\Repositories\AutoWateringScenarioSwitchStateRepository;

class AutoWateringScenarioSwitchStateService
{
    private AutoWateringScenarioSwitchStateRepository $repository;

    public function __construct(AutoWateringScenarioSwitchStateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll(int $id): ?array
    {
        return $this->repository->getSwiftMap($id);
    }

    public function update(int $id, Request $request): void
    {
        $this->repository->update($id, $request);
        $this->forgetCache();
    }

    public function getScenarioSwifts(int $scenarioId): array
    {
        return $this->repository->getScenarioSwifts($scenarioId);
    }

    private function forgetCache(): void
    {
        App::call(
            function (AutoWateringService $service) {
                $service->forgetCache();
            }
        );
    }
}
