<?php

namespace Modules\AutoWatering\Entities;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class AutoWateringScenarioSwitchState
 *
 * @mixin Builder
 * @property string $scenario_id
 * @property string $relay_id
 * @property int $state
 * @property int $created_at
 * @property int $updated_at
 */
class AutoWateringScenarioSwitchState extends Model
{
    use HasFactory;

    public const ACTIVATE = 1;
    public const DEACTIVATE = 0;

    protected $table = 'auto_watering_scenario_switch_state';
}
