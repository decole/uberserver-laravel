<?php

namespace Modules\AutoWatering\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Relay\Entities\Relay;

/**
 * Class AutoWateringSchedule
 *
 * @mixin Builder
 * @property int $id
 * @property string $name
 * @property string $next_start
 * @property string $date_next_start
 * @property string $date_last_normal_on
 * @property string $date_last_normal_off
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class AutoWateringSchedule extends Model
{
    use HasFactory;

    /**
     *  date_start - will change after switching to another processor
     *  date_end - date_start next switch
     *
     *  states:
     *  0 - disabled processing
     *  1 - ready for processing (state after processing the processor - turning off everything)
     *  2 - enabled, running (only one processor can be used)
     *  3 - normally disabled (switching off according to the inclusion of the next processor)
     *  4 - alarm (if the valve state does not change after the control signal. the state is stored using the cache)
     *        - notification and deactivation of all topics
     */

    public const STATE_DEACTIVATE = 0;
    public const STATE_STAND_BY_ON = 1;
    public const STATE_NORMALLY_ON = 2;
    public const STATE_NORMALLY_OFF = 3;
    public const STATE_EMERGENCY_SHUTDOWN = 4;

    protected $fillable = [
        'name',
        'next_start',
        'date_next_start',
        'date_last_normal_on',
        'date_last_normal_off',
        'status',
    ];

    public static function getStatuses(): array
    {
        return [
            self::STATE_DEACTIVATE => 'Выключен (не активен)',
            self::STATE_STAND_BY_ON => 'Готов к включению',
            self::STATE_NORMALLY_ON => 'Нормально включен',
            self::STATE_NORMALLY_OFF => 'Нормально выключен',
            self::STATE_EMERGENCY_SHUTDOWN => 'Аварийно выключен',
        ];
    }

    public function relays(): BelongsToMany
    {
        return $this->belongsToMany(Relay::class,
            'auto_watering_scenario_switch_state',
            'scenario_id',
            'relay_id'
        );
    }

    protected static function newFactory()
    {
        return \Modules\AutoWatering\Database\factories\AutoWateringScheduleFactory::new();
    }
}
