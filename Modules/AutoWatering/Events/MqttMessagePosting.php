<?php

namespace Modules\AutoWatering\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class MqttMessagePosting
{
    use Dispatchable, SerializesModels;

    public string $topic;
    public string $payload;

    public function __construct(string $topic, string $payload)
    {
        $this->topic = $topic;
        $this->payload = $payload;
    }

    public function broadcastOn()
    {
        return [];
    }
}
