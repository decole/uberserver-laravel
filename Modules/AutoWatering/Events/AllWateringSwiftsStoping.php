<?php

namespace Modules\AutoWatering\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AllWateringSwiftsStoping
{
    use Dispatchable, SerializesModels;

    public function broadcastOn()
    {
        return [];
    }
}
