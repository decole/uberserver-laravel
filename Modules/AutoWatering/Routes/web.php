<?php

use Illuminate\Support\Facades\Route;

Route::prefix('autowatering')->group(function() {
    Route::get('/', 'AutoWateringController@index')->name('auto_watering.index')->middleware('auth');
    Route::get('/create', 'AutoWateringController@create')->name('auto_watering.create')->middleware('auth');
    Route::post('/store', 'AutoWateringController@store')->name('auto_watering.store')->middleware('auth');
    Route::get('/edit/{id}', 'AutoWateringController@edit')->name('auto_watering.edit')->where('id', '[0-9]+')->middleware('auth');
    Route::post('/update/{id}', 'AutoWateringController@update')->name('auto_watering.update')->where('id', '[0-9]+')->middleware('auth');
    Route::get('/destroy/{id}', 'AutoWateringController@destroy')->name('auto_watering.destroy')->where('id', '[0-9]+')->middleware('auth');

    Route::get('/swifts/{id}', 'AutoWateringSwiftController@index')->name('auto_watering.swifts.show')->where('id', '[0-9]+')->middleware('auth');
    Route::post('/swifts/update/{id}', 'AutoWateringSwiftController@update')->name('auto_watering.swifts.update')->where('id', '[0-9]+')->middleware('auth');
});
