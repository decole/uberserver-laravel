<?php

namespace Modules\AutoWatering\Tests\Feature;

use App\Events\NewAlertNotify;
use DateTime;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Modules\AutoWatering\Entities\AutoWateringScenarioSwitchState;
use Modules\AutoWatering\Entities\AutoWateringSchedule;
use Modules\AutoWatering\Events\AllWateringSwiftsStoping;
use Modules\AutoWatering\Events\MqttMessagePosting;
use Modules\AutoWatering\Services\ValidationService;
use Modules\Relay\Entities\Relay;
use Mosquitto\Message;
use Tests\TestCase;

class AutoWateringValidationServiceTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    public function testValidationService(): void
    {
        $service = new ValidationService();
        $reflection = new \ReflectionClass(get_class($service));
        $method = $reflection->getMethod('getCheckSwiftTopics');
        $method->setAccessible(true);
        $service = new ValidationService();
        $list = $method->invokeArgs($service, []);


        $this->assertIsObject($service);
        $this->assertIsArray($list);
        $this->assertEquals(true, in_array('water/check/major', $list));
    }

    public function testSuccessValidation(): void
    {
        Event::fake();

        $message = $this->prepareMajorWateringSwiftMessage(0);
        $this->prepareSuccessScenarioSwiftOne();

        $service = new ValidationService();
        $state = $service->validateMessage($message);

        $this->assertEquals(true, $state);
        Event::assertNotDispatched(MqttMessagePosting::class);
    }

    /**
     * where datetime run scenario = current datetime - ignore check state - transition process
     */
    public function testDelayValidation(): void
    {
        Cache::flush();
        Event::fake();

        $message = $this->prepareMajorWateringSwiftMessage(1);
        $this->prepareSuccessScenarioSwiftOne(AutoWateringSchedule::STATE_NORMALLY_ON);

        $service = new ValidationService();
        $state = $service->validateMessage($message);

        $this->assertEquals(true, $state);
        Event::assertNotDispatched(MqttMessagePosting::class);
        Event::assertNotDispatched(NewAlertNotify::class);
    }

    public function testWrongValidation(): void
    {
        Cache::flush();
        Event::fake();

        $message = $this->prepareMajorWateringSwiftMessage(1);
        $needTime = (new DateTime())->modify('-2 hour')->format('Y-m-d H:i:s');
        $this->prepareSuccessScenarioSwiftOne(AutoWateringSchedule::STATE_NORMALLY_ON, $needTime);

        $service = new ValidationService();
        $state = $service->validateMessage($message);
        $this->assertEquals(false, $state);
        Event::assertDispatched(AllWateringSwiftsStoping::class);
    }

    public function testAlarmStopAll(): void
    {
        Cache::flush();
        Event::fake();

        $service = new ValidationService();
        $reflection = new \ReflectionClass(get_class($service));
        $method = $reflection->getMethod('alarmStopAll');
        $method->setAccessible(true);
        $service = new ValidationService();
        $method->invokeArgs($service, []);


        $this->assertIsObject($service);
        Event::assertDispatched(AllWateringSwiftsStoping::class);
    }

    public function testIsNormalState(): void
    {
        Cache::flush();
        Event::fake();
        $this->prepareSuccessScenarioSwiftOne();

        $message = $this->prepareMajorWateringSwiftMessage(1);
        $scenario = AutoWateringSchedule::where('name', '=', 'Клапан 1')->first();
        $this->prepareStateMajorSwift(AutoWateringScenarioSwitchState::ACTIVATE, $scenario->id);


        $service = new ValidationService();
        $state = $service->isNormalState($message, $scenario);
        $this->assertEquals(true, $state);

        Cache::flush();
        $message = $this->prepareMajorWateringSwiftMessage(0);
        $scenario = AutoWateringSchedule::where('name', '=', 'Клапан 1')->first();
        $this->prepareStateMajorSwift(AutoWateringScenarioSwitchState::DEACTIVATE, $scenario->id);
        $state = $service->isNormalState($message, $scenario);
        $this->assertEquals(false, $state);
    }

    private function prepareMajorWateringSwiftMessage(string $payload): Message
    {
        $message = new Message();
        $message->topic = 'water/check/major';
        $message->payload = $payload;

        return $message;
    }

    private function prepareSuccessScenarioSwiftOne(?int $state = null, ?string $date = null): void
    {
        if (!$state) {
            $state = AutoWateringSchedule::STATE_STAND_BY_ON;
        }

        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }

        $model = AutoWateringSchedule::where('name', '=', 'Клапан 1')->first();

        if ($model) {
            $model->status = $state;
            $model->date_next_start = $date;
            $model->next_start = '+ 1 day';
            $model->save();

            return;
        }

        $model = new AutoWateringSchedule();
        $model->name = 'Клапан 1';
        $model->next_start = '+ 1 day';
        $model->date_next_start = $date;
        $model->date_last_normal_on = $date;
        $model->date_last_normal_off = $date;
        $model->status = $state;
        $model->created_at = $date;
        $model->updated_at = $date;

        $model->save();
    }

    private function prepareStateMajorSwift(int $state, int $scenarioId): void
    {
        $relay = Relay::where('topic', '=', 'water/major')->first();

        if (!$relay) {
            $relay = new Relay();
            $relay->id = 100;
            $relay->name = 'Главный клапан';
            $relay->topic = 'water/major';
            $relay->check_topic = 'water/check/major';
            $relay->command_on = '1';
            $relay->command_off = '0';
            $relay->last_command = '0';
            $relay->message_info = '0';
            $relay->message_ok = '0';
            $relay->message_warn= '0';
            $relay->type= '1';
            $relay->status= '0';
            $relay->notify= '0';
            $relay->created_at= '2022-02-15 20:03:03';
            $relay->updated_at= '2022-02-15 20:03:03';
            $relay->save();
        }

        $switchScenarioState = AutoWateringScenarioSwitchState::where('scenario_id', '=', $scenarioId)
            ->where('relay_id', '=', $relay->id)
            ->first();

        if (!$switchScenarioState) {
            DB::table('auto_watering_scenario_switch_state')->insert([
                'scenario_id' => $scenarioId,
                'relay_id' => $relay->id,
                'state' => $state,
                'created_at' => null,
                'updated_at' => null,
            ]);
        }
    }
}
