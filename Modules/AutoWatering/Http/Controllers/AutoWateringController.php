<?php

namespace Modules\AutoWatering\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller;
use Modules\AutoWatering\Http\Requests\CreateAutoWateringScenarioRequest;
use Modules\AutoWatering\Services\AutoWateringService;

class AutoWateringController extends Controller
{
    private AutoWateringService $service;

    public function __construct(AutoWateringService $service)
    {
        $this->service = $service;
    }

    /**
     * @return Application|Factory|View
     */
    public function index(): Renderable
    {
        $scenarios = $this->service->getAll();

        return view('autowatering::index', compact('scenarios'));
    }

    /**
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('autowatering::create');
    }

    /**
     * @param CreateAutoWateringScenarioRequest $request
     * @return RedirectResponse
     */
    public function store(CreateAutoWateringScenarioRequest $request): RedirectResponse
    {
        $validated = $request->validated();
        $this->service->create($validated);

        return redirect()->route('auto_watering.index')->with('success','Scenario created successfully.');
    }

    /**
     * @param int $id
     * @return Renderable
     */
    public function edit(int $id): Renderable
    {
        $scenario = $this->service->getOne($id);

        return view('autowatering::edit', compact('scenario'));
    }

    /**
     * @param CreateAutoWateringScenarioRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(CreateAutoWateringScenarioRequest $request, int $id): RedirectResponse
    {
        $validated = $request->validated();
        $update = $this->service->update($id, $validated);

        if (!$update) {
            return redirect()->route('auto_watering.index')
                ->with('error', 'Scenario not updated');
        }

        return redirect()->route('auto_watering.index')
            ->with('success','Scenario ' . $id . ' updated successfully');
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $delete = $this->service->delete($id);

        if (!$delete) {
            return redirect()->route('auto_watering.index')
                ->with('error', 'Relay not found');
        }

        return redirect()->route('auto_watering.index')
            ->with('success','Relay deleted successfully');
    }
}
