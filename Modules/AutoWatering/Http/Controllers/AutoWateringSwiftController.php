<?php

namespace Modules\AutoWatering\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AutoWatering\Services\AutoWateringScenarioSwitchStateService;
use Modules\AutoWatering\Services\AutoWateringService;

class AutoWateringSwiftController extends Controller
{
    private AutoWateringScenarioSwitchStateService $service;
    private AutoWateringService $scenarioService;

    public function __construct(AutoWateringScenarioSwitchStateService $service, AutoWateringService $scenarioService)
    {
        $this->service = $service;
        $this->scenarioService = $scenarioService;
    }

    /**
     * @param int $id
     * @return Application|Factory|View
     */
    public function index(int $id): Renderable
    {
        $scenario = $this->scenarioService->getOne($id);
        $swifts = $this->service->getAll($id);

        return view('autowatering::swifts.index', [
            'swifts' => $swifts,
            'scenario' => $scenario,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $this->service->update($id, $request);

        return redirect()->route('auto_watering.swifts.show', $id)
            ->with('success','Scenario ' . $id . ' - state swifts updated successfully');
    }
}
