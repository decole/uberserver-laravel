<?php

namespace Modules\AutoWatering\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAutoWateringScenarioRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'next_start' => 'required|max:255',
            'date_next_start' => [
                'date_format:Y-m-d H:i:s',
            ],
            'status'  => 'required|integer|between:0,4', // see AutoWateringSchedule::getStatuses()
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
