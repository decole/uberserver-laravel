<?php

namespace Modules\AutoWatering\Console;

use DateTime;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Modules\AutoWatering\Entities\AutoWateringSchedule;
use Modules\AutoWatering\Services\AutoWateringService;

class WateringCommand extends Command
{
    protected $name = 'watering:schedule';

    protected $description = 'Auto watering run scheduler for watering scenarios';

    private AutoWateringService $service;

    public function __construct(AutoWateringService $service)
    {
        $this->service = $service;

        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        while (true) {
            $scenarios = $this->service->getAll();
            $currentTime = time();

            foreach ($scenarios as $scenario) {
                $timeStartScenario = $this->getTimeStamp($scenario->date_next_start);

                if ($currentTime >= $timeStartScenario) {
                    $this->changeScenariosState($scenario, $scenarios);
                }
            }

            $delay = 60 - date('s');
            sleep($delay);
        }

        $this->info(date('Y-m-d H:i:s') . ' end watering handler work process');
    }

    /**
     * @param AutoWateringSchedule $scenarioUpping
     * @param AutoWateringSchedule[] $scenarios
     * @return void
     * @throws Exception
     */
    private function changeScenariosState(AutoWateringSchedule $scenarioUpping, array $scenarios): void
    {
        $isEmergencyShutdown = false;

        foreach ($scenarios as $scenario) {
            if ($scenario->status == AutoWateringSchedule::STATE_EMERGENCY_SHUTDOWN) {
                $isEmergencyShutdown = true;

                break;
            }

            if ($scenario->status == AutoWateringSchedule::STATE_DEACTIVATE) {
                $nextDate = $this->getNextDateTime($scenario->next_start, $scenario->date_next_start);
                $this->service->changeScenarioNextStartDate($scenario->id, $nextDate);

                continue;
            }

            if ($scenario->status == AutoWateringSchedule::STATE_NORMALLY_ON) {
                var_dump('change scenario ' . $scenario->name .  ' to stand by');
                $nextDate = $this->getNextDateTime($scenario->next_start, $scenario->date_next_start);

                $this->service->changeScenarioNextStartDate($scenario->id, $nextDate);
                $this->service->changeScenarioState($scenario->id, AutoWateringSchedule::STATE_STAND_BY_ON);
            }
        }

        if ($isEmergencyShutdown) {
            $this->sendNotify('Обнаружен  аварийно завершенный сенарий!');

            return;
        }

        $this->startScenario($scenarioUpping);

        $this->service->forgetCache();
    }

    private function startScenario(AutoWateringSchedule $scenario): void
    {
        $this->service->ExecuteCommandSwifts($scenario->id);
        var_dump('execute scenario ' . $scenario->name);
        $nextDate = $this->getNextDateTime($scenario->next_start, $scenario->date_next_start);

        $this->service->changeScenarioNextStartDate($scenario->id, $nextDate);
        $this->service->changeScenarioState($scenario->id, AutoWateringSchedule::STATE_NORMALLY_ON);
    }

    /**
     * @throws Exception
     */
    private function getNextDateTime(?string $modify, ?string $currentDate = null): string
    {
        $currentDate = $currentDate === null ? date('Y-m-d H:i:00') : $currentDate;

        if (!$modify) {
            return $currentDate;
        }

        $date = new DateTime($currentDate);
        $date->modify($modify);

        return $date->format('Y-m-d H:i:00');
    }

    private function getTimeStamp(string $date): int
    {

        return (new DateTime($date))->getTimestamp();
    }

    private function sendNotify(string $message): void
    {
        App::call(
            function (AutoWateringService $service) use ($message){
                $service->sendNotify($message);
            }
        );
    }
}
