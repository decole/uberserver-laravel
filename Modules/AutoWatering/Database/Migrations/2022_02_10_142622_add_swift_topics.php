<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Modules\AutoWatering\Entities\AutoWateringSchedule;
use Modules\AutoWatering\Services\RunServices\FifthSwitchRunProcessor;
use Modules\AutoWatering\Services\RunServices\FirstSwitchRunProcessor;
use Modules\AutoWatering\Services\RunServices\FourthSwitchRunProcessor;
use Modules\AutoWatering\Services\RunServices\SecondSwitchRunProcessor;
use Modules\AutoWatering\Services\RunServices\StopAllSwitchRunProcessor;
use Modules\AutoWatering\Services\RunServices\ThirdSwitchRunProcessor;

class AddSwiftTopics extends Migration
{
    public function up()
    {
        $values = [
            [
                'id' => 1,
                'name' => 'Клапан 1',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 10:00:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 2,
                'name' => 'Клапан 1-2',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 10:30:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 3,
                'name' => 'Клапан 2',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 10:31:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 4,
                'name' => 'Клапан 2-3',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 11:30:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 5,
                'name' => 'Клапан 3',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 11:31:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 6,
                'name' => 'Клапан 3-4',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 12:00:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 7,
                'name' => 'Клапан 4',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 12:01:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 8,
                'name' => 'Клапан 4-5',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 12:30:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 9,
                'name' => 'Клапан 5',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 12:31:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
            [
                'id' => 10,
                'name' => 'Отключить все клапана',
                'next_start' => '+3 days',
                'date_next_start' => date('Y-m-d 13:00:s'),
                'date_last_normal_on' => date('Y-m-d H:i:s'),
                'date_last_normal_off' => date('Y-m-d H:i:s'),
                'status' => AutoWateringSchedule::STATE_DEACTIVATE,
            ],
        ];

        DB::table('auto_watering_schedules')->truncate();

        foreach ($values as $value) {
            DB::table('auto_watering_schedules')->insert($value);
        }

       $relayValues = [
            [
                'id' => 7,
                'name' => 'Главный клапан',
                'topic' => 'water/major',
                'check_topic' => 'water/check/major',
                'command_on' => '1',
                'command_off' => '0',
                'check_topic_payload_on' => '1',
                'check_topic_payload_off' => '0',
                'last_command' => '1',
                'message_info' => 'Главный клапан',
                'message_ok' => 'Главный клапан - {value}',
                'message_warn' => 'Состояние главного клапана неизвестно - {value}',
                'type' => '1',
                'status' => '0',
                'notify' => '0',
                'created_at' => '2022-02-15 20:03:03',
                'updated_at' => '2022-02-15 20:03:03',
            ],
            [
                'id' => 8,
                'name' => 'Клапан 1',
                'topic' => 'water/relay1',
                'check_topic' => 'water/check/relay1',
                'command_on' => '1',
                'command_off' => '0',
                'check_topic_payload_on' => '1',
                'check_topic_payload_off' => '0',
                'last_command' => '1',
                'message_info' => 'Клапан 1',
                'message_ok' => 'Клапан 1 - {value}',
                'message_warn' => 'Состояние клапана 1 неизвестно - {value}',
                'type' => '1',
                'status' => '0',
                'notify' => '0',
                'created_at' => '2022-02-15 20:03:03',
                'updated_at' => '2022-02-15 20:03:03',
            ],
            [
                'id' => 9,
                'name' => 'Клапан 2',
                'topic' => 'water/relay2',
                'check_topic' => 'water/check/relay2',
                'command_on' => '1',
                'command_off' => '0',
                'check_topic_payload_on' => '1',
                'check_topic_payload_off' => '0',
                'last_command' => '1',
                'message_info' => 'Клапан 1',
                'message_ok' => 'Клапан 2 - {value}',
                'message_warn' => 'Состояние клапана 2 неизвестно - {value}',
                'type' => '1',
                'status' => '0',
                'notify' => '0',
                'created_at' => '2022-02-15 20:03:03',
                'updated_at' => '2022-02-15 20:03:03',
            ],
            [
                'id' => 10,
                'name' => 'Клапан 3',
                'topic' => 'water/relay3',
                'check_topic' => 'water/check/relay3',
                'command_on' => '1',
                'command_off' => '0',
                'check_topic_payload_on' => '1',
                'check_topic_payload_off' => '0',
                'last_command' => '1',
                'message_info' => 'Клапан 3',
                'message_ok' => 'Клапан 3 - {value}',
                'message_warn' => 'Состояние клапана 3 неизвестно - {value}',
                'type' => '1',
                'status' => '0',
                'notify' => '0',
                'created_at' => '2022-02-15 20:03:03',
                'updated_at' => '2022-02-15 20:03:03',
            ],
            [
                'id' => 11,
                'name' => 'Клапан 4',
                'topic' => 'water/relay4',
                'check_topic' => 'water/check/relay4',
                'command_on' => '1',
                'command_off' => '0',
                'check_topic_payload_on' => '1',
                'check_topic_payload_off' => '0',
                'last_command' => '1',
                'message_info' => 'Клапан 4',
                'message_ok' => 'Клапан 4 - {value}',
                'message_warn' => 'Состояние клапана 4 неизвестно - {value}',
                'type' => '1',
                'status' => '0',
                'notify' => '0',
                'created_at' => '2022-02-15 20:03:03',
                'updated_at' => '2022-02-15 20:03:03',
            ],
            [
                'id' => 12,
                'name' => 'Клапан 5',
                'topic' => 'water/relay5',
                'check_topic' => 'water/check/relay5',
                'command_on' => '1',
                'command_off' => '0',
                'check_topic_payload_on' => '1',
                'check_topic_payload_off' => '0',
                'last_command' => '1',
                'message_info' => 'Клапан 5',
                'message_ok' => 'Клапан 5 - {value}',
                'message_warn' => 'Состояние клапана 5 неизвестно - {value}',
                'type' => '1',
                'status' => '0',
                'notify' => '0',
                'created_at' => '2022-02-15 20:03:03',
                'updated_at' => '2022-02-15 20:03:03',
            ],
        ];
        foreach ($relayValues as $value) {
            DB::table('relays')->insert($value);
        }
    }

    public function down()
    {
        DB::table('auto_watering_schedules')->truncate();
        DB::table('auto_watering_scenario_switch_state')->truncate();

        $relayValues = [
            'water/major',
            'water/relay1',
            'water/relay2',
            'water/relay3',
            'water/relay4',
            'water/relay5',
        ];

        foreach ($relayValues as $value) {
            DB::table('relays')->where('topic', '=', $value)->delete();
        }
    }
}
