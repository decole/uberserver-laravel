<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoWateringSchedulesTable extends Migration
{
    public function up()
    {
        Schema::create('auto_watering_schedules', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('next_start')->index();
            $table->dateTime('date_next_start');
            $table->dateTime('date_last_normal_on');
            $table->dateTime('date_last_normal_off');
            $table->tinyInteger('status', false, true)->default(0);
            $table->timestamps();
        });

        Schema::create('auto_watering_scenario_switch_state', function (Blueprint $table) {
            $table->integer('scenario_id');
            $table->foreign('scenario_id')
                ->references('id')->on('auto_watering_schedules')
                ->onDelete('cascade');
            $table->integer('relay_id');
            $table->foreign('relay_id')
                ->references('id')->on('relays')
                ->onDelete('cascade');
            $table->tinyInteger('state', false, true)->default(0);
            $table->timestamps();

            $table->index(['scenario_id', 'relay_id'], 'auto_watering_scenario_switch_state_scenario_id_relay_id_index');
        });
    }

    public function down()
    {
        Schema::table('auto_watering_scenario_switch_state', function (Blueprint $table) {
            $table->dropForeign(['relay_id']);
            $table->dropColumn('relay_id');
            $table->dropForeign(['scenario_id']);
            $table->dropColumn('scenario_id');
        });

        Schema::dropIfExists('auto_watering_schedules');
        Schema::dropIfExists('auto_watering_scenario_switch_state');
    }
}
