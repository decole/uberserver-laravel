<?php

namespace Modules\AutoWatering\Database\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Relay\Entities\Relay;
use Modules\AutoWatering\Entities\AutoWateringScenarioSwitchState;
use Mosquitto\Message;

class AutoWateringScenarioSwitchStateRepository
{
    public function getSwiftMap(int $scenarioId): array
    {
        $map = [];
        $swifts = $this->getScenarioSwifts($scenarioId);

        /** @var Relay[] $swifts */
        foreach ($swifts as $swift) {
            $map[$swift->id] = [
                'name' => $swift->name,
                'state' => $swift->state ?? AutoWateringScenarioSwitchState::DEACTIVATE, // if the state of the valve is unknown - the state is off
            ];
        }

        return $map;
    }

    /**
     * Need module Relay
     *
     * @param int $scenarioId
     * @return array
     */
    public function getScenarioSwifts(int $scenarioId): array
    {
        return Relay::where('relays.type','=',Relay::TYPE_SWIFT)
            ->leftJoin('auto_watering_scenario_switch_state', function ($join) use ($scenarioId) {
                $join->on('relays.id', '=', 'auto_watering_scenario_switch_state.relay_id')
                    ->on('auto_watering_scenario_switch_state.scenario_id', '=', DB::raw($scenarioId));
            })
            ->get()
            ->all();
    }

    public function update(int $scenarioId, Request $request): void
    {
        $service = new AutoWateringRepository();
        /** @var array $turnOnSwifts */
        $turnOnSwifts = $request->request->get('status');
        $scenario = $service->getOne($scenarioId);
        $swifts = $this->getSwiftMap($scenarioId);

        if (!$turnOnSwifts) {
            return;
        }

        $swiftIds = array_keys($swifts);
        $scenario->relays()->sync($swiftIds);

        foreach ($swifts as $id => $swift) {
            $state = AutoWateringScenarioSwitchState::DEACTIVATE;

            if (isset($turnOnSwifts[$id]) && $turnOnSwifts[$id] === 'on') {
                $state = AutoWateringScenarioSwitchState::ACTIVATE;
            }

            // may be ude DB::update
            $scenario->relays()->updateExistingPivot($id, ['auto_watering_scenario_switch_state.state' => $state]);
        }
    }

    public function isSwitchState(int $scenarioId, Message $message): bool
    {
        $swift = Relay::where('relays.type','=',Relay::TYPE_SWIFT)
            ->where('relays.check_topic', '=', $message->topic)
            ->leftJoin('auto_watering_scenario_switch_state', function ($join) use ($scenarioId) {
                $join->on('relays.id', '=', 'auto_watering_scenario_switch_state.relay_id')
                    ->on('auto_watering_scenario_switch_state.scenario_id', '=', DB::raw($scenarioId));
            })
            ->get()
            ->first();

        if (!$swift) {
            Log::error('При проверке клапана автополива с топиком ' . $message->topic
                . ' произошла ошибка. Клапан с таки топиком не найден.');
        }

        if ($swift->state === null) {
            $swiftScenarioState = 0;
        } else {
            $swiftScenarioState = (int)$swift->state;
        }

        $swiftReceivedPayload = $message->payload;
        /** @var Relay $swift */
        $swiftScenarioPayload = $swiftScenarioState === AutoWateringScenarioSwitchState::ACTIVATE ? $swift->check_topic_payload_on : $swift->check_topic_payload_off;

        if ($swiftReceivedPayload != $swiftScenarioPayload) {
            Log::error('Критическая ошибка. Текущее состояние реле');
            return false;
        }

        return true;
    }
}
