<?php

namespace Modules\AutoWatering\Database\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\AutoWatering\Entities\AutoWateringSchedule;
use Modules\Relay\Database\Repositories\RelayRepository;
use Modules\Relay\Entities\Relay;

class AutoWateringRepository
{
    /**
     * @return AutoWateringSchedule[]
     */
    public function getAll(): array
    {
        return AutoWateringSchedule::orderBy('date_next_start', 'ASC')->get()->all();
    }

    /**
     * @return Relay[]
     */
    public function getAllSwifts(): array
    {
        return (new RelayRepository())->getAllByType(Relay::TYPE_SWIFT);
    }

    public function getOne(int $id): ?AutoWateringSchedule
    {
        return AutoWateringSchedule::where('id', $id)->first();
    }

    public function create(array $fields): void
    {
        $id = AutoWateringSchedule::find(DB::table('auto_watering_schedules')->max('id'))->id ?? 1;

        $scenario = new AutoWateringSchedule();
        $scenario->id = ++$id;
        $scenario->name = $fields['name'];
        $scenario->next_start = $fields['next_start'];
        $scenario->date_next_start = $fields['date_next_start'];
        $scenario->status = $fields['status'];
        $scenario->date_last_normal_on = $fields['date_last_normal_on'];
        $scenario->date_last_normal_off = $fields['date_last_normal_off'];
        $scenario->save();
    }

    public function update(int $id, array $fields): bool
    {
        $scenario = $this->getOne($id);

        if (!$scenario) {
            return false;
        }

        return $scenario->update($fields);
    }

    public function delete(array $ids): ?bool
    {
        return AutoWateringSchedule::whereIn('id', $ids)->delete();
    }

    public function changeScenarioState(int $scenarioId, int $statement): void
    {
        if (!in_array($statement, array_keys(AutoWateringSchedule::getStatuses()))) {
            Log::error('Система пыталась изменить состояние сценария автополива на неизвестное состояние - ' . $statement);

            return;
        }

        AutoWateringSchedule::where('id', $scenarioId)
            ->update(['status'=> $statement]);
    }

    public function changeScenarioNextStartDate(int $scenarioId, string $nextDate): void
    {
        AutoWateringSchedule::where('id', $scenarioId)
            ->update(['date_next_start'=> $nextDate]);
    }
}
