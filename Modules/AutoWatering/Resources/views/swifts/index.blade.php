@extends('layouts.app')

@section('content')
    <h1 class="mb-4" >Настройки сценария автополива - {{ $scenario->name }}</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="POST" action="{{ route('auto_watering.swifts.update', $scenario->id) }}">
                    <div class="card-header">
                        <h3 class="card-title">Сценарий - {{ $scenario->name }}</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <a href="{{ route('auto_watering.index') }}" class="btn btn-primary">Назад</a>&nbsp;
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                            @csrf
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-warning">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Следующее включение</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($swifts as $id => $swift)
                                    <tr>
                                        <td>{{ $swift['name'] }}</td>
        {{--                                <td>{{ $swift['state'] }}</td>--}}
                                        <td>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" name="status[{{ $id }}]" @if($swift['state'] == 1) checked @endif id="statusSwitch-{{ $id }}">
                                                <label class="custom-control-label" for="statusSwitch-{{ $id }}">Статус клапана</label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
