@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>Регистрация нового сценария автополива</h1>
            </div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create Relay</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('auto_watering.store') }}">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Название реле</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Реле 01 дом">
                </div>
                <div class="form-group">
                    <label>Выражение следующего запуска</label>
                    <input type="text" name="next_start" value="@if(!old('next_start')){{ '+ 3 days' }}@else {{ old('next_start') }} @endif" class="form-control" placeholder="+ 3 days">
                </div>
                <div class="form-group">
                    <label>Дата и время следующего запуска</label>
                    <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                        <input type="text" id="datetimepicker" name="date_next_start" value="{{ old('date_next_start') }}" class="form-control datetimepicker-input" data-target="#reservationdatetime" />
                        <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Статус сценария</label>
                    <select class="form-control select2" name="status" style="width: 100%;">
                        @foreach (\Modules\AutoWatering\Entities\AutoWateringSchedule::getStatuses() as $key => $type)
                            <option value="{{ $key }}" @if($key === old('status')) selected="selected" @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
@endsection

@section('third_party_scripts')
    <script>
        $(document).ready(function() {
            jQuery('#datetimepicker').datetimepicker({
                format: "Y-m-d H:i:s",
            });
        });
    </script>
    <link href="{{ asset('css/jquery.datetimepicker.min.css') }}" rel="stylesheet">
@endsection
