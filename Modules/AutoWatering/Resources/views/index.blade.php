@extends('layouts.app')

@section('content')
    <h1 class="mb-4" >Настройки модуля Автополив ({!! config('autowatering.name') !!})</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Список зарегистрированных сенсоров</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <a href="{{ route('auto_watering.create') }}" type="submit" class="btn btn-primary">Создать</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                        <div class="alert alert-warning">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Время включения</th>
                            <th>Следующий период</th>
                            <th>Клапана</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($scenarios as $scenario)
                            <tr>
                                <td>{{ $scenario->name }}</td>
                                <td>{{ $scenario->date_next_start }}</td>
                                <td>{{ $scenario->next_start }}</td>
                                <td><a href="{{ route('auto_watering.swifts.show', ['id' => $scenario->id]) }}" class="btn btn-warning">Настроить клапана</a></td>
                                <td>
                                    @if ($scenario->status == \Modules\AutoWatering\Entities\AutoWateringSchedule::STATE_EMERGENCY_SHUTDOWN)
                                        <strong style="color: red;">
                                    @endif
                                    {{ \Modules\AutoWatering\Entities\AutoWateringSchedule::getStatuses()[$scenario->status] }}
                                    @if ($scenario->status == \Modules\AutoWatering\Entities\AutoWateringSchedule::STATE_EMERGENCY_SHUTDOWN)
                                        </strong>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('auto_watering.edit', ['id' => $scenario->id]) }}" class="btn btn-warning">Изменить</a>
                                    <a href="{{ route('auto_watering.destroy', ['id' => $scenario->id]) }}" class="btn btn-danger">Удалить</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
