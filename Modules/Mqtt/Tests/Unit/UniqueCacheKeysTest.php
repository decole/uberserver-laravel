<?php

namespace Modules\Mqtt\Tests\Unit;

use Modules\Mqtt\Entities\MqttCacheKeysRegister;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UniqueCacheKeysTest extends TestCase
{
    public function testDuplicateCacheKeys()
    {
        $lists = MqttCacheKeysRegister::CACHE_KEY_REGISTRY;
        $allKeys = [];

        foreach ($lists as $keys) {
            $allKeys = array_merge($allKeys, $keys);
        }

        $countAllKeys = count($allKeys);
        $uniqueKeys = count(array_unique($allKeys));

        $this->assertEquals($countAllKeys, $uniqueKeys);
    }
}
