<?php

namespace Modules\Mqtt\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\AutoWatering\Events\MqttMessagePosting;
use Modules\Mqtt\Listeners\PostMqttMessage;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        MqttMessagePosting::class => [
            PostMqttMessage::class,
        ],
    ];
}
