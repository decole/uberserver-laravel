<?php

namespace Modules\Mqtt\Console;

use Illuminate\Console\Command;
use Modules\Mqtt\Services\MqttService;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MqttCommand extends Command
{
    protected $signature = 'mqtt:run';

    protected $description = 'Run mqtt parse service';

    private MqttService $service;

    public function __construct(MqttService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    public function handle()
    {
        // flush all cache
        Cache::flush();

        $this->info('Run mqtt service');
        $this->service->listen();
    }

    protected function getArguments(): array
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    protected function getOptions(): array
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
