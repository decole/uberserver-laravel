<?php

namespace Modules\Mqtt\Presenters;

use App\Presenters\Interfaces\PresenterInterface;
use Modules\Sensor\Entities\Sensor;

class GetSensorPayloadPresenter implements PresenterInterface
{
    private ?Sensor $sensor;
    private array $payloads;

    public function __construct(?Sensor $sensor, array $cachedPayloads)
    {
        $this->sensor = $sensor;
        $this->payloads = $cachedPayloads;
    }

    public function present(): array
    {
        if (!$this->sensor) {
            return [ 'payload' => null ];
        }

        return [
            'payload' => $this->payloads[$this->sensor->topic] ?? null,
        ];
    }
}
