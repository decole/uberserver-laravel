<?php

namespace Modules\Mqtt\Presenters;

use App\Presenters\Interfaces\PresenterInterface;
use Modules\Sensor\Entities\Sensor;

class GetSensorByTypePresenter implements PresenterInterface
{
    private ?Sensor $sensor;
    private array $payloads;

    public function __construct(?Sensor $sensor, array $cachedPayloads)
    {
        $this->sensor = $sensor;
        $this->payloads = $cachedPayloads;
    }

    public function present(): array
    {
        if (!$this->sensor) {
            return [ 'result' => null ];
        }

        $result = [
            'name' => $this->sensor->name,
            'topic' => $this->sensor->topic,
            'payload' => $this->payloads[$this->sensor->topic] ?? null,
            'type' => Sensor::TYPES[$this->sensor->type],
            'status' => (bool)$this->sensor->status,
            'notify' => (bool)$this->sensor->notify,
        ];

        if ($this->sensor->type != 2) {
            $payloadMinMax = [
                'payload_min' => $this->sensor->payload_min,
                'payload_max' => $this->sensor->payload_max,
            ];

            $result = array_merge($result, $payloadMinMax);
        }

        return [
            'result' => $result,
        ];
    }
}
