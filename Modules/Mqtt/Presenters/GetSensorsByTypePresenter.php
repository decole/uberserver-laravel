<?php

namespace Modules\Mqtt\Presenters;

use App\Presenters\Interfaces\PresenterInterface;
use Modules\Sensor\Entities\Sensor;

class GetSensorsByTypePresenter implements PresenterInterface
{
    private array $sensors;
    private array $payloads;

    public function __construct(array $sensors, array $cachedPayloads)
    {
        $this->sensors = $sensors;
        $this->payloads = $cachedPayloads;
    }

    public function present(): array
    {
        $sensors = [];

        foreach ($this->sensors as $sensor) {
            $result = [
                'name' => $sensor->name,
                'topic' => $sensor->topic,
                'payload' => $this->payloads[$sensor->topic] ?? null,
                'type' => Sensor::TYPES[$sensor->type],
                'status' => (bool)$sensor->status,
                'notify' => (bool)$sensor->notify,
            ];

            if ($sensor->type != 2) {
                $payloadMinMax = [
                    'payload_min' => $sensor->payload_min,
                    'payload_max' => $sensor->payload_max,
                ];

                $result = array_merge($result, $payloadMinMax);
            }

            $sensors[] = $result;
        }

        return $sensors;
    }
}
