<?php

namespace Modules\Mqtt\Services;

use Modules\Mqtt\Events\MqttMessagePublished;
use Illuminate\Support\Facades\Cache;
use Mosquitto\Client;
use Mosquitto\Message;

class MqttService
{
    // https://mosquitto-php.readthedocs.io/en/latest/client.html#Mosquitto\Client::onConnect

    private string $host;

    private int $port;

    private Client $client;

    private bool $isConnect = false;

    private const CACHE_LIMIT = 120;

    private const CACHE_TOPIC_LIST = 'mqtt_topic_list';

    public function __construct()
    {
        $this->host = config('mqtt.mqtt_broker_ip');
        $this->port = config('mqtt.mqtt_broker_port');
        $this->client = new Client();
    }

    /**
     * Service initializing
     */
    public function listen(): void
    {
        $this->connectClient();
        $this->registerClient();

        while (true) {
            $this->client->loop(5);
        }
    }

    /**
     * Calculate function by mqtt message payload receive
     */
    public function process(Message $message): void
    {
        MqttMessagePublished::dispatch($message);
        $this->setCache($message);
        $this->setTopicList($message);
    }

    /**
     * Disconnect mqtt connection in lib
     */
    public function disconnect(): void
    {
        if ($this->isConnect) {
            $this->client->disconnect();
        }
    }

    public function getTopicList(): ?array
    {
        return Cache::get(static::CACHE_TOPIC_LIST, []);
    }

    public function getTopicState(string $topic): ?string
    {
        return Cache::get($topic);
    }

    public function post(string $topic, ?string $payload): ?string
    {
        $this->connectClient();
        $this->client->publish($topic, $payload, 1, 0);

        return $payload;
    }

    private function connectClient(): void
    {
        $this->client->connect($this->host, $this->port, 5);
    }

    private function registerClient(): void
    {
        $this->client->onConnect(function ($rc) {
            $this->isConnect = $rc === 0;
        });

        $this->client->onDisconnect(function () {
            $this->isConnect = false;
        });

        $this->client->subscribe('#', 1);
        $this->client->onMessage([$this, 'process']);

        register_shutdown_function([$this, 'disconnect']);
    }

    private function setCache(Message $message): void
    {
        Cache::put($message->topic, $message->payload, static::CACHE_LIMIT);
    }

    private function setTopicList(Message $message): void
    {
        $list = Cache::get(static::CACHE_TOPIC_LIST, []);

        if (!in_array($message->topic,$list)) {
            $list = array_merge($list, [$message->topic]);
        }

        Cache::put(static::CACHE_TOPIC_LIST, $list);
    }
}
