<?php

namespace Modules\Mqtt\Services;

use Illuminate\Support\Facades\Cache;
use Modules\Sensor\Entities\Sensor;

class MqttApiFlutterService
{
    /**
     * @return Sensor[]
     */
    public function getSensorsByType(string $type): array
    {
        $convertType = array_flip(Sensor::TYPES)[$type];

        return Sensor::where('type', '=', $convertType)->get()->all();
    }

    /**
     * @return Sensor[]
     */
    public function getAllSensors(): array
    {
        return Sensor::get()->all();
    }

    /**
     * @param Sensor[] $sensors
     * @return array
     */
    public function getSensorsByTypePayloads(array $sensors): array
    {
        if (empty($sensors)) {
            return [];
        }

        $payloads = [];

        foreach ($sensors as $sensor) {
            if (!$sensor) {
                $payloads['none'] = null;

                continue;
            }

            $payloads[$sensor->topic] = Cache::get($sensor->topic);
        }

        return $payloads;
    }

    public function getSensorByTopic(string $topic): ?Sensor
    {
        return Sensor::where('topic', '=', $topic)
            ->get()
            ->first();
    }
}
