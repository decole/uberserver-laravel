<?php

namespace Modules\Mqtt\Entities;

use Modules\AutoWatering\Services\AutoWateringService;
use Modules\AutoWatering\Services\ValidationService as AutowateringServiceAlias;
use Modules\Relay\Services\ValidationService as RelayServiceAlias;
use Modules\Sensor\Services\ValidationService as SensorServiceAlias;

class MqttCacheKeysRegister
{
    public const CACHE_KEY_REGISTRY = [
        SensorServiceAlias::class => [
            SensorServiceAlias::SENSOR_CACHE_KEY,
            SensorServiceAlias::SENSOR_TOPICS_CACHE_KEY,
        ],
        RelayServiceAlias::class => [
            RelayServiceAlias::RELAY_CACHE_KEY,
            RelayServiceAlias::RELAY_CHECK_TOPICS_CACHE_KEY,
            RelayServiceAlias::RELAY_TOPICS_CACHE_KEY,
        ],
        AutowateringServiceAlias::class => [
            AutowateringServiceAlias::SWIFT_TOPICS_CACHE_KEY,
            AutowateringServiceAlias::SWIFTS_CACHE_KEY,
            AutowateringServiceAlias::SWIFTS_CHECK_TOPICS_CACHE_KEY,
            AutowateringServiceAlias::SWIFT_COLLECTION_TOPICS_CACHE_KEY,
        ],
        AutoWateringService::class => [
            AutoWateringService::NOTIFY_CACHE_KEY,
        ],
    ];
}
