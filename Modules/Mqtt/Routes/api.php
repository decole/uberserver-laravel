<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('mqtt')->group(function() {
    Route::get('/', 'MqttApiController@index');
    Route::get('/topic', 'MqttApiController@topic');
    Route::get('/topics', 'MqttApiController@topics');
    Route::post('/send', 'MqttApiController@send');
});

Route::prefix('flutter')->group(function() {
    Route::middleware('auth.bearer.api')->get('/', function () { return response()->json(['ping' => 'pong!'], 200); });
    Route::middleware('auth.bearer.api')->get('/sensors/types', 'MqttApiFlutterController@types');
    Route::middleware('auth.bearer.api')->get('/sensors/list', 'MqttApiFlutterController@sensorList');
    Route::middleware('auth.bearer.api')->post('/sensors/type', 'MqttApiFlutterController@sensorsByTypes');
    Route::middleware('auth.bearer.api')->post('/sensors/topic', 'MqttApiFlutterController@sensorByTopic');
    Route::middleware('auth.bearer.api')->post('/sensors/topic/payload', 'MqttApiFlutterController@sensorPayload');
});
