<?php

namespace Modules\Mqtt\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Mqtt\Presenters\GetSensorByTypePresenter;
use Modules\Mqtt\Presenters\GetSensorPayloadPresenter;
use Modules\Mqtt\Presenters\GetSensorsByTypePresenter;
use Modules\Mqtt\Services\MqttApiFlutterService;
use Modules\Sensor\Entities\Sensor;
use Illuminate\Http\Request;

/**
 * Документация https://docs.google.com/document/d/1zSAPPkrVBT37OmFYYMEbMS7vM90Mhz46d2_8awP5Zmc/edit?usp=sharing
 */
class MqttApiFlutterController extends Controller
{
    private MqttApiFlutterService $service;

    public function __construct(MqttApiFlutterService $service)
    {
        $this->service = $service;
    }

    /**
     * 1. получить все типы датчиков - перечислить типы
     *
     * @return JsonResponse
     */
    public function types(): JsonResponse
    {
        $result = ['types' => Sensor::TYPES];

        return response()->json($result, 200);
    }

    /**
     * 2. получить датчики по конкретному типу
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sensorsByTypes(Request $request): JsonResponse
    {
        $type = $request->get('type');

        if (!$type) {
            return response()->json($this->getError('Нет поля type в теле запроса'), 400);
        }

        if (!in_array($type, Sensor::TYPES)) {
            return response()->json($this->getError('Передано неподдерживаемое значение поля type'), 400);
        }

        $sensors = $this->service->getSensorsByType($type);
        $payloads = $this->service->getSensorsByTypePayloads($sensors);
        $result = (new GetSensorsByTypePresenter($sensors, $payloads))->present();

        return response()->json($result, 200);
    }

    /**
     * 3. перечислить все датчики
     *
     * @return JsonResponse
     */
    public function sensorList(): JsonResponse
    {
        $sensors = $this->service->getAllSensors();
        $payloads = $this->service->getSensorsByTypePayloads($sensors);
        $result = (new GetSensorsByTypePresenter($sensors, $payloads))->present();

        return response()->json($result, 200);
    }

    /**
     * 4. получение по конкретному идентификатору датчика (топику) данные о нем
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sensorByTopic(Request $request): JsonResponse
    {
        $topic = $request->get('topic');

        if (!$topic) {
            return response()->json($this->getError('Нет поля topic в теле запроса'), 400);
        }

        $sensor = $this->service->getSensorByTopic($topic);
        $payloads = $this->service->getSensorsByTypePayloads([$sensor]);
        $result = (new GetSensorByTypePresenter($sensor, $payloads))->present();

        return response()->json($result, 200);
    }

    /**
     * 5. получение значение сенсора (payload) по конкретному идентификатору датчика (топику - topic)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sensorPayload(Request $request): JsonResponse
    {
        $topic = $request->get('topic');

        if (!$topic) {
            return response()->json($this->getError('Нет поля topic в теле запроса'), 400);
        }

        $sensor = $this->service->getSensorByTopic($topic);
        $payloads = $this->service->getSensorsByTypePayloads([$sensor]);
        $result = (new GetSensorPayloadPresenter($sensor, $payloads))->present();

        return response()->json($result, 200);
    }

    /**
     * @param string $message
     * @return string[]
     */
    private function getError(string $message): array
    {
        return [
          'error' => $message,
        ];
    }
}
