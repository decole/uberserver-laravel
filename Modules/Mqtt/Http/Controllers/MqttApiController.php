<?php

namespace Modules\Mqtt\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Modules\Mqtt\Services\MqttService;

class MqttApiController extends Controller
{
    public function index(): JsonResponse
    {
        $hello = ['Congrats' => 'Hello there!'];

        return response()->json($hello, 200);
    }

    public function topic(Request $request): JsonResponse
    {
        $topic = $request->get('topic');

        if (!$topic) {
            return response()->json(['error' => 'param topic not found'], 404);
        }

        $response = ['payload' => Cache::get($topic)];

        return response()->json($response, 200);
    }

    public function topics(Request $request): JsonResponse
    {
        $topics = $request->get('topics');

        if (!$topics) {
            return response()->json(['error' => 'param topics not found'], 404);
        }

        $list = explode(',', $topics);
        $response = [];

        foreach ($list as $topic) {
            $response[$topic] = Cache::get($topic);
        }

        return response()->json($response, 200);
    }

    public function send(Request $request, MqttService $service): JsonResponse
    {
        $topic = $request->get('topic', null);
        $payload = $request->get('payload', null);

        if ($topic === null || $payload === null) {
            return response()->json(['error' => 'param topic or payload not found'], 404);
        }

        $service->post($topic, $payload);
        $response = ['succes' => true];

        return response()->json($response, 200);
    }
}
