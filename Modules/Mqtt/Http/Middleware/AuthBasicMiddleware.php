<?php

namespace Modules\Mqtt\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthBasicMiddleware
{
    private const TOKEN = 'c2NoYW9zQHlhbmRleC5ydTpzY2hhb3M=';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->isCorrectBearerToken($request)) {
            return response()->json(['error' => 'Auth failed'], 401);
        } else {
            return $next($request);
        }
    }

    private function bearerToken(Request $request): ?string
    {
        $header = $request->header('Authorization', '');
        if (Str::startsWith($header, 'Bearer ')) {
            return Str::substr($header, 7);
        }
    }

    private function isCorrectBearerToken(Request $request): bool
    {
        $token = $this->bearerToken($request);

        return self::TOKEN === $token;
    }
}
