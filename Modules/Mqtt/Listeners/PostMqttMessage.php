<?php

namespace Modules\Mqtt\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\AutoWatering\Events\MqttMessagePosting;
use Modules\Mqtt\Services\MqttService;

class PostMqttMessage
{
    private MqttService $service;

    public function __construct(MqttService $service)
    {
        $this->service = $service;
    }

    public function handle(MqttMessagePosting $event)
    {
        $this->service->post($event->topic, $event->payload);
    }
}
