<?php

use Illuminate\Http\Request;

Route::prefix('alice_home')->group(function() {
    Route::any('/v1.0/', 'AliceSmartHomeController@index'); // Проверка доступности Endpoint URL провайдера
    Route::post('/v1.0/user/unlink', 'AliceSmartHomeController@unlink'); // Оповещение о разъединении аккаунтов
    Route::get('/v1.0/user/devices', 'AliceSmartHomeController@devices'); // Информация об устройствах пользователя
    Route::post('/v1.0/user/devices/query', 'AliceSmartHomeController@query'); // Информация о состояниях устройств пользователя
    Route::post('/v1.0/user/devices/action', 'AliceSmartHomeController@action'); // Изменение состояния у устройств
});
