<?php

namespace Modules\AliceSmartHome\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\AliceSmartHome\Presenters\DeviceListQueryPresenter;
use Modules\AliceSmartHome\Services\SmartHomeService;
use Exception;

class AliceSmartHomeController extends Controller
{
    private SmartHomeService $service;

    public function __construct(SmartHomeService $service)
    {
        $this->service = $service;
    }

    public function index(): JsonResponse
    {
        return response()->json(['success' => true], 200);
    }

    public function unlink(Request $request): JsonResponse
    {
        $requestId = $this->service->getRequestId($request);

        return response()->json(['request_id' => $requestId]);
    }

    public function devices(Request $request): JsonResponse
    {
        $requestId = $this->service->getRequestId($request);

        $result = [
            'request_id' => $requestId,
            'payload' => [
                'user_id' => 'decole2014',
                'devices' => [
                    [
                        'id' =>  'switcher1',
                        'name' =>  'switcher1',
                        'type' =>  'devices.types.switch',
                        'capabilities' => [
                            [
                                'type' => 'devices.capabilities.on_off',
                                'retrievable' => true
                            ]
                        ],
                    ],
                ]
            ]
        ];

        return response()->json($result);
    }

    public function query(Request $request): JsonResponse
    {
        $requestId = $this->service->getRequestId($request);
        $devices = $this->service->devicesQuery($request->getContent());
        $presenter = new DeviceListQueryPresenter($devices, $requestId);

        return response()->json($presenter->present());
    }

    /**
     * @throws Exception
     */
    public function action(Request $request): JsonResponse
    {
        $content = file_get_contents('php://input');

        $this->saveToLog($content);

        $query = json_decode($content);

        $requestId = $this->service->getRequestId($request);

        $topic = 'margulis/lamp01';

        $state = $this->service->relayAction($topic, $query);

        $result = [
            "request_id" => $requestId,
            "payload" => [
                "user_id" => "decole2014",
                "devices" => [
                    [
                        "id" =>  '1',
                        "name" =>  'switcher1',
                        "type" =>  'devices.types.switch',
                        "capabilities" => [
                            [
                                "type" => "devices.capabilities.on_off",
                                "retrievable" => true,
                                "state" => [
                                    'instance' => 'on',
                                    "value" => $state,
                                    "action_result" => [
                                        "status" => "DONE"
                                    ],
                                ],
                            ]
                        ],
                    ]
                ]
            ]
        ];

        return response()->json($result);
    }

    private function saveToLog($content): void
    {
        Log::stack(['alice', 'stdout'])->info($content);
    }
}
