<?php

namespace Modules\AliceSmartHome\Services;

use Exception;
use Modules\AliceSmartHome\Services\Devices\DeviceInterface;
use Modules\AliceSmartHome\Services\Devices\LampMargulisDevice;

class DeviseService
{
    public array $devices = [];

    public function __construct()
    {
        $this->devices = [
            new LampMargulisDevice(),
        ];
    }

    public function getDevice(string $id): DeviceInterface
    {
        foreach ($this->devices as $device) {
            if ($device->getDevice()->id == $id) {
                return $device;
            }
        }

        throw new Exception('device not found on device list');
    }
}
