<?php

namespace Modules\AliceSmartHome\Services\Dto;

use Modules\AliceSmartHome\Services\Devices\Schemas\SchemaInterface;

class DeviceDto
{
    public string $id;

    public string $type;

    /**
     * @var mixed $state
     */
    public $state;

    public SchemaInterface $schema;
}
