<?php

namespace Modules\AliceSmartHome\Services\Devices;

use Illuminate\Support\Facades\Cache;
use Modules\AliceSmartHome\Services\Devices\Schemas\RelaySchema;
use Modules\AliceSmartHome\Services\Dto\DeviceDto;

class LampMargulisDevice extends AbstractDevice implements DeviceInterface
{
    public function getDevice(): DeviceDto
    {
        $id = 'switcher1';
        $topic = 'margulis/check/lamp01';
        $state = Cache::get($topic) == 1 ? true : false;

        $device = new DeviceDto();
        $device->id = $id;
        $device->state = $state;
        $device->type = self::RELAY;
        $device->schema = new RelaySchema($id, $state);

        return $device;
    }
}
