<?php

namespace Modules\AliceSmartHome\Services\Devices;

class AbstractDevice
{
    public const TYPES = [
        self::SENSOR,
        self::RELAY,
    ];

    public const SENSOR = 'sensor';

    public const RELAY = 'relay';
}
