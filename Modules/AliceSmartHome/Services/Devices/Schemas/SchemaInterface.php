<?php

namespace Modules\AliceSmartHome\Services\Devices\Schemas;

interface SchemaInterface
{
    public function getSchema(): array;
}
