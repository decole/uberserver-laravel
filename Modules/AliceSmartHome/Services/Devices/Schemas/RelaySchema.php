<?php

namespace Modules\AliceSmartHome\Services\Devices\Schemas;

class RelaySchema implements SchemaInterface
{
    public function __construct(string $id, bool $state)
    {
        $this->id = $id;
        $this->state = $state;
    }

    public function getSchema(): array
    {
        return [
            "id" => $this->id,
            "capabilities" => [
                [
                    "type" => "devices.capabilities.on_off",
                    "retrievable" => true,
                    "state" => [
                        'instance' => 'on',
                        "value" => $this->state,
                        "action_result" => [
                            "status" => "DONE"
                        ],
                    ],
                ]
            ],
        ];
    }
}
