<?php

namespace Modules\AliceSmartHome\Services\Devices;

use Modules\AliceSmartHome\Services\Dto\DeviceDto;

interface DeviceInterface
{
    public function getDevice(): DeviceDto;
}
