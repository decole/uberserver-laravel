<?php

namespace Modules\Relay\Listeners;

use Modules\AutoWatering\Events\AllWateringSwiftsStoping;
use Modules\AutoWatering\Services\AutoWateringService;

class AllSwiftStopListener
{
    private AutoWateringService $service;

    public function __construct(AutoWateringService $service)
    {
        $this->service = $service;
    }

    public function handle(AllWateringSwiftsStoping $event)
    {
        $this->service->alarmStopAll();
    }
}
