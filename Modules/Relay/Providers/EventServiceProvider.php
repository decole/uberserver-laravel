<?php

namespace Modules\Relay\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\AutoWatering\Events\AllWateringSwiftsStoping;
use Modules\Mqtt\Events\MqttMessagePublished;
use Modules\Relay\Listeners\AllSwiftStopListener;
use Modules\Relay\Listeners\NewMqttMessage;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        MqttMessagePublished::class => [
            NewMqttMessage::class,
        ],
        AllWateringSwiftsStoping::class => [
            AllSwiftStopListener::class,
        ]
    ];
}
