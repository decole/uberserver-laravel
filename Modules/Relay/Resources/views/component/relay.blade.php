<div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="far fa-hdd"></i></span>
    <div class="info-box-content">
        <span class="info-box-text">{{ $title }}</span>
        <div class="btn-group relay-control" data-swift-topic="{{ $topic }}" data-swift-topic-check="{{ $check_topic }}" data-swift-id="{{ $id }}" data-swift-on="{{ $command_on }}" data-swift-off="{{ $command_off }}">
            <button type="button" class="btn btn-outline-success active" data-swift-topic="{{ $topic }}" data-swift-check="{{ $check_command_on }}" value="{{ $command_on }}">On</button>
            <button type="button" class="btn btn-outline-danger" data-swift-topic="{{ $topic }}" data-swift-check="{{ $check_command_off }}" value="{{ $command_off }}">Off</button>
        </div>
    </div>
</div>
