@extends('layouts.app')

@section('content')
    <h1 class="mb-4" >Настройки модуля Реле ({!! config('relay.name') !!})</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Список зарегистрированных реле</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <a href="{{ route('relay.create') }}" type="submit" class="btn btn-primary">Создать</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                        <div class="alert alert-warning">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($relays as $relay)
                            <tr>
                                <td>{{ $relay->id }}</td>
                                <td>{{ $relay->name }}</td>
                                <td>{{ \Modules\Relay\Entities\Relay::TYPES[$relay->type] }}</td>
                                <td>{{ $relay->status ? 'Active' : 'Deactive' }}</td>
                                <td>
                                    <a href="{{ route('relay.edit', ['id' => $relay->id]) }}" class="btn btn-warning">Изменить</a>
                                    <a href="{{ route('relay.destroy', ['id' => $relay->id]) }}" class="btn btn-danger">Удалить</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
