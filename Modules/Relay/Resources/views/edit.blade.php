@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Изменение реле {{ $relay->name }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">General Form</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit relay {{ $relay->name }} [{{ $relay->id }}]</h3>
        </div>

        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('relay.update', $relay->id) }}">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Название реле</label>
                    <input type="text" name="name" value="{{ $relay->name }}" class="form-control" placeholder="Реле 01 дом">
                </div>
                <div class="form-group">
                    <label>Топик</label>
                    <input type="text" name="topic" value="{{ $relay->topic }}" class="form-control" placeholder="hall/relay01">
                </div>
                <div class="form-group">
                    <label>Проверочный топик</label>
                    <input type="text" name="check_topic" value="{{ $relay->check_topic }}" class="form-control" placeholder="hall/check/relay01">
                </div>
                <div class="form-group">
                    <label>Команда включить</label>
                    <input type="text" name="command_on" value="{{ $relay->command_on }}" class="form-control" placeholder="1">
                </div>
                <div class="form-group">
                    <label>Команда выключить</label>
                    <input type="text" name="command_off" value="{{ $relay->command_off }}" class="form-control" placeholder="0">
                </div>
                <div class="form-group">
                    <label>Проверочная команда включено</label>
                    <input type="text" name="check_topic_payload_on" value="{{ $relay->check_topic_payload_on }}" class="form-control" placeholder="1">
                </div>
                <div class="form-group">
                    <label>Проверочная команда выключено</label>
                    <input type="text" name="check_topic_payload_off" value="{{ $relay->check_topic_payload_off }}" class="form-control" placeholder="0">
                </div>
                <div class="form-group">
                    <label>Последняя команда</label>
                    <input type="text" name="last_command" value="{{ $relay->last_command }}" class="form-control" placeholder="1">
                </div>
                <div class="form-group">
                    <label>Текст для информации</label>
                    <input type="text" name="message_info" value="{{ $relay->message_info }}" class="form-control" placeholder="Реле01 дом">
                </div>
                <div class="form-group">
                    <label>Текст для статуса Oк</label>
                    <input type="text" name="message_ok" value="{{ $relay->message_ok }}" class="form-control" placeholder="Реле01 дом, работает">
                </div>
                <div class="form-group">
                    <label>Текст для вывода алерта</label>
                    <input type="text" name="message_warn" value="{{ $relay->message_warn }}" class="form-control" placeholder="Внимание, что то произошло">
                </div>
                <div class="form-group">
                    <label>Тип сенсора</label>
                    <select class="form-control select2" name="type" style="width: 100%;">
{{--                        <option selected="selected">Alabama</option>--}}
                        @foreach (\Modules\Relay\Entities\Relay::TYPES as $key => $type)
                            <option value="{{ $key }}" @if($key === $relay->type) selected="selected" @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="status" @if($relay->status) checked @endif id="statusSwitch">
                    <label class="custom-control-label" for="statusSwitch">Статус реле</label>
                </div>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="notify" @if($relay->notify) checked @endif id="notifySwitch">
                    <label class="custom-control-label" for="notifySwitch">Присылать оповещения</label>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
@endsection

@section('third_party_scripts')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
@endsection
