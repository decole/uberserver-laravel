<?php

namespace Modules\Relay\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Relay\Http\Requests\CreateRelayRequest;
use Modules\Relay\Http\Requests\UpdateRelayRequest;
use Modules\Relay\Services\RelayService;

class RelayController extends Controller
{
    private RelayService $service;

    public function __construct(RelayService $service)
    {
        $this->service = $service;
    }
    public function index()
    {
        $relays = $this->service->getAll();

        return view('relay::index', compact('relays'));
    }

    public function create()
    {
        return view('relay::create');
    }

    public function store(CreateRelayRequest $request)
    {
        $validated = $request->validated();
        $this->service->create($validated);

        return redirect()->route('relay.index')->with('success','Relay created successfully.');
    }

    /**
     * not used
     */
    public function show($id)
    {
        return view('relay::show');
    }

    public function edit($id)
    {
        $relay = $this->service->getOne($id);

        return view('relay::edit', compact('relay'));
    }

    public function update(UpdateRelayRequest $request, $id)
    {
        $validated = $request->validated();
        $update = $this->service->update($id, $validated);

        if (!$update) {
            return redirect()->route('relay.index')
                ->with('error', 'Sensor not updated');
        }

        return redirect()->route('relay.index')
            ->with('success','Relay ' . $id . ' updated successfully');
    }

    public function destroy($id)
    {
        $delete = $this->service->delete($id);

        if (!$delete) {
            return redirect()->route('relay.index')
                ->with('error', 'Relay not found');
        }

        return redirect()->route('relay.index')
            ->with('success','Relay deleted successfully');
    }
}
