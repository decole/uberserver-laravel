<?php

namespace Modules\Relay\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRelayRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:relays|max:300',
            'topic' => 'required|unique:relays|max:400',
            'check_topic' => 'required|unique:relays|max:400',
            'command_on' => 'required|max:255',
            'command_off' => 'required|max:255',
            'check_topic_payload_on' => 'required|max:255',
            'check_topic_payload_off' => 'required|max:255',
            'last_command' => 'nullable',
            'message_info' => 'required|max:255',
            'message_ok' => 'required|max:255',
            'message_warn' => 'required|max:255',
            'type'  => 'required|integer|between:0,1', // see Relay::TYPES
            'status' => 'nullable',
            'notify' => 'nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
