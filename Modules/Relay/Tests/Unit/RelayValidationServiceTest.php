<?php

namespace Modules\Relay\Tests\Unit;

use App\Events\NewAlertNotify;
use Illuminate\Support\Facades\Cache;
use Modules\AutoWatering\Events\MqttMessagePosting;
use Modules\Relay\Entities\Relay;
use Mosquitto\Message;
use Modules\Relay\Services\ValidationService;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RelayValidationServiceTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    public function  setUp(): void
    {
        parent::setUp();

        $this->service = new ValidationService();
        $this->prepareTestRelay();
    }

    public function testValidationServiceCheckOkRelay()
    {
        Event::fake();
        $relay = Relay::where('topic', 'test-relay')->first();
        $message = $this->createMessage($relay->check_topic, $relay->check_topic_payload_on);
        $this->service->validateMessage($message);

        $this->assertIsObject($this->service->getRelays());
        Event::assertNotDispatched(MqttMessagePosting::class);
    }

    public function testValidationServiceCheckNotOkRelay()
    {
        Cache::flush();
        Event::fake();
        $relay = Relay::where('topic', 'test-relay')->first();
        $message = $this->createMessage($relay->check_topic, 'kek');
        $this->service->validateMessage($message);

        $this->assertIsObject($this->service->getRelays());
        $this->assertEquals(true, $this->service->isCheckTopicRelay($relay->check_topic));
        $this->assertEquals(false, $this->service->isTopicRelay($relay->check_topic));
        $this->assertEquals(true, $this->service->isTopicRelay($relay->topic));
        $this->assertEquals(false, $this->service->isCheckTopicRelay($relay->topic));
        Event::assertDispatched(NewAlertNotify::class);
        Event::assertDispatchedTimes(NewAlertNotify::class, 1);
    }

    protected function createMessage(string $topic, string $payload):  Message
    {
        $message = new Message();
        $message->topic = $topic;
        $message->payload = $payload;

        return $message;
    }

    protected function prepareTestRelay()
    {
        $relay = Relay::where('topic', 'test-relay')->first();

        if (!$relay) {
            $relay = new Relay();
            $relay->name = 'test relay';
            $relay->topic = 'test-relay';
            $relay->check_topic = 'check/test-relay';
            $relay->command_on = '1';
            $relay->command_off = '0';
            $relay->check_topic_payload_on = '1';
            $relay->check_topic_payload_off = '0';
            $relay->last_command = '0';
            $relay->message_info = 'message_info';
            $relay->message_ok = 'message_ok';
            $relay->message_warn = 'message_warn';
            $relay->type = 0;
            $relay->status = 1;
            $relay->notify = 1;
            $relay->save();
        }
    }
}
