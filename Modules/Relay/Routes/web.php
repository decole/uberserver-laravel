<?php

Route::prefix('relay')->group(function() {
    Route::get('/', 'RelayController@index')->name('relay.index')->middleware('auth');
    Route::get('/create', 'RelayController@create')->name('relay.create')->middleware('auth');
    Route::post('/store', 'RelayController@store')->name('relay.store')->middleware('auth');
    Route::get('/show/{id}', 'RelayController@show')->name('relay.show')->where('id', '[0-9]+')->middleware('auth');
    Route::get('/edit/{id}', 'RelayController@edit')->name('relay.edit')->where('id', '[0-9]+')->middleware('auth');
    Route::post('/update/{id}', 'RelayController@update')->name('relay.update')->where('id', '[0-9]+')->middleware('auth');
    Route::get('/destroy/{id}', 'RelayController@destroy')->name('relay.destroy')->where('id', '[0-9]+')->middleware('auth');
});
