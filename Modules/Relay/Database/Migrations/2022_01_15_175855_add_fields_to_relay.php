<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToRelay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relays', function (Blueprint $table) {
            $table->string('check_topic_payload_on')->default(1)->after('payload');
            $table->string('check_topic_payload_off')->default(0)->after('payload');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relays', function (Blueprint $table) {
            $table->dropColumn('check_topic_payload_on');
            $table->dropColumn('check_topic_payload_off');
        });
    }
}
