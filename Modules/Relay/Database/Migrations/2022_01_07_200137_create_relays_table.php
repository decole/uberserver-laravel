<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relays', function (Blueprint $table) {
            $table->id();
            $table->string('name', '300')->default('');
            $table->string('topic', '400')->unique();
            $table->string('check_topic', '400')->unique();
            $table->string('command_on')->default('');
            $table->string('command_off')->default('');
            $table->string('last_command')->default('');
            $table->string('message_info')->default('');
            $table->string('message_ok')->default('');
            $table->string('message_warn')->default('');
            $table->integer('type')->default(0);
            $table->tinyInteger('status', false, true)->default(0);
            $table->tinyInteger('notify', false, true)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relays');
    }
}
