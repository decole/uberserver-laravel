<?php

namespace Modules\Relay\Database\Repositories;

use Modules\Relay\Entities\Relay;

class RelayRepository
{
    public function getOne(int $id): ?Relay
    {
        return Relay::where('id', $id)->first();
    }

    public function getAll(): array
    {
        return Relay::orderBy('id', 'ASC')
            ->get()
            ->all();
    }

    public function getAllByType(int $type): array
    {
        if (in_array($type, Relay::TYPES)) {
            return [];
        }

        return Relay::where('type', $type)
            ->orderBy('id', 'ASC')
            ->get()
            ->all();
    }

    public function create(array $fields): void
    {
        Relay::create($fields);
    }

    public function update(int $id, array $fields): bool
    {
        $sensor = $this->getOne($id);

        if (!$sensor) {
            return false;
        }

        return $sensor->update($fields);
    }

    public function delete(array $ids): ?bool
    {
        return Relay::whereIn('id', $ids)->delete();
    }
}
