<?php

namespace Modules\Relay\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Relay
 *
 * @mixin Builder
 * @property int $id
 * @property string $name
 * @property string $topic
 * @property string $check_topic
 * @property string $command_on
 * @property string $command_off
 * @property string $check_topic_payload_on
 * @property string $check_topic_payload_off
 * @property string $last_command
 * @property string $message_info
 * @property string $message_ok
 * @property string $message_warn
 * @property int $type
 * @property int $status
 * @property int $notify
 * @property int $created_at
 * @property int $updated_at
 */
class Relay extends Model
{
    use HasFactory;

    public const TYPE_RELAY = 0;
    public const TYPE_SWIFT = 1;

    public const TYPES = [
        self::TYPE_RELAY => 'relay',
        self::TYPE_SWIFT => 'vent' // клапан
    ];

    protected $fillable = [
        'name',
        'topic',
        'check_topic',
        'command_on',
        'command_off',
        'check_topic_payload_on',
        'check_topic_payload_off',
        'last_command',
        'message_info',
        'message_ok',
        'message_warn',
        'type',
        'status',
        'notify',
    ];

    protected static function newFactory()
    {
        return \Modules\Relay\Database\factories\RelayFactory::new();
    }
}
