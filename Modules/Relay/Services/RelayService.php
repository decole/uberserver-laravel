<?php

namespace Modules\Relay\Services;

use Modules\Relay\Database\Repositories\RelayRepository;
use Modules\Relay\Entities\Relay;
use Illuminate\Support\Facades\Cache;

class RelayService
{
    private RelayRepository $repository;

    public function __construct(RelayRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getOne(int $id): ?Relay
    {
        return $this->repository->getOne($id);
    }

    /**
     * @return Relay[]|array
     */
    public function getAll(): array
    {
        return $this->repository->getAll();
    }

    public function create(array $fields): void
    {
        $fields['status'] = isset($fields['status']) ? 1 : 0;
        $fields['notify'] = isset($fields['notify']) ? 1 : 0;

        $this->repository->create($fields);
        $this->forgetCache();
    }

    public function update(int $id, array $fields): bool
    {
        $fields['status'] = isset($fields['status']) ? 1 : 0;
        $fields['notify'] = isset($fields['notify']) ? 1 : 0;

        $status = $this->repository->update($id, $fields);
        $this->forgetCache();

        return $status;
    }

    public function delete(int $id): ?bool
    {
        $status = $this->repository->delete([$id]);
        $this->forgetCache();

        return $status;
    }

    private function forgetCache(): void
    {
        Cache::forget(ValidationService::RELAY_CACHE_KEY);
        Cache::forget(ValidationService::RELAY_TOPICS_CACHE_KEY);
        Cache::forget(ValidationService::RELAY_CHECK_TOPICS_CACHE_KEY);
    }
}
