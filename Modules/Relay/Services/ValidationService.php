<?php

namespace Modules\Relay\Services;

use App\Dto\NotifyDto;
use App\Events\NewAlertNotify;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Modules\Relay\Entities\Relay;
use Mosquitto\Message;

class ValidationService
{
    public const CACHE_TIME = 9000;
    public const RELAY_CACHE_KEY = 'relays';
    public const RELAY_TOPICS_CACHE_KEY = 'relay-topics';
    public const RELAY_CHECK_TOPICS_CACHE_KEY = 'relays-check-topics';

    public function validateMessage(Message $message): void
    {
        if ($this->isTopicRelay($message->topic) || $this->isCheckTopicRelay($message->topic)) {
            $relays = $this->getRelays();
            $payload = $message->payload;

            /** @var Relay[] $relays */
            foreach ($relays as $relay) {
                if ($message->topic == $relay->topic && $relay->status == 1) {
                    if (($payload != $relay->command_on && $payload != $relay->command_off) &&
                        $relay->notify == 1
                    ) {
                        $this->sendNotify($relay->message_warn . ' - ' . $payload);
                    }
                }
                if ($message->topic == $relay->check_topic && $relay->status == 1) {

                    if (($payload != $relay->check_topic_payload_on && $payload != $relay->check_topic_payload_off) &&
                        $relay->notify == 1
                    ) {
                        $this->sendNotify($relay->message_warn . '. Проверочный топик имеет состояние - ' . $payload);
                    }
                }
            }
        }
    }

    private function sendNotify(string $message): void
    {
        $dto = new NotifyDto();
        $dto->topic = 'relay';
        $dto->message = $message;

        NewAlertNotify::dispatch($dto);
    }

    public function getRelays(): Collection
    {
        return Cache::remember(self::RELAY_CACHE_KEY, static::CACHE_TIME, function () {
            return Relay::all();
        });
    }

    public function isTopicRelay(string $topic): bool
    {
        $topics = Cache::remember(self::RELAY_TOPICS_CACHE_KEY, static::CACHE_TIME, function () {
            $relays = $this->getRelays();
            $topics = [];

            /** @var Relay $relay */
            foreach ($relays as $relay) {
                $topics[] = $relay->topic;
            }

            return $topics;
        });

        return in_array($topic, $topics);
    }

    public function isCheckTopicRelay(string $topic): bool
    {
        $topics = Cache::remember(self::RELAY_CHECK_TOPICS_CACHE_KEY, static::CACHE_TIME, function () {
            $relays = $this->getRelays();
            $topics = [];

            /** @var Relay $relay */
            foreach ($relays as $relay) {
                $topics[] = $relay->check_topic;
            }

            return $topics;
        });

        return in_array($topic, $topics);
    }
}
