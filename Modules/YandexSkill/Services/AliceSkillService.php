<?php

namespace Modules\YandexSkill\Services;

use Modules\YandexSkill\Services\Dialogs\DiagnosticDialog;
use Modules\YandexSkill\Services\Dialogs\FireSecureDialog;
use Modules\YandexSkill\Services\Dialogs\HelloDialog;
use Modules\YandexSkill\Services\Dialogs\LampDialog;
use Modules\YandexSkill\Services\Dialogs\PingDialog;
use Modules\YandexSkill\Services\Dialogs\SecureDialog;
use Modules\YandexSkill\Services\Dialogs\StatusDialog;
use Modules\YandexSkill\Services\Dialogs\WateringDialog;
use Modules\YandexSkill\Services\Dialogs\WeatherDialog;

class AliceSkillService
{
    /**
     * @var mixed
     */
    public $text;

    /**
     * @var mixed[]|mixed
     */
    public $message;

    /**
     * @var string[]
     */
    private array $listing;

    public function __construct(?array $request_json)
    {
        $this->text = 'Привет';
        $this->message = $request_json;
        $this->listing = [
            'ping' => new PingDialog(),
            'hello' => new HelloDialog(),
            'lamp' => new LampDialog(),
            'weather' => new WeatherDialog(),
//            'watering' => new WateringDialog(),
//            'secure' => new SecureDialog(),
//            'diagnose' => new DiagnosticDialog(),
//            'status' => new StatusDialog(),
//            'fire' => new FireSecureDialog(),
        ];
    }

    public function route(): void
    {
        if (is_array($this->message)) {
            foreach ($this->message as $value)
            {
                if($this->sorter($value)) {
                    break;
                }
            }
        }
        else {
            $this->sorter($this->message);
        }
    }

    private function sorter($verb): bool
    {
        foreach ($this->listing as $value) {
            if (in_array($verb, $value->listVerb())) {
                $this->text = $value->process($this->message);
                return true;
            }
        }

        return false;
    }
}
