<?php

namespace Modules\YandexSkill\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\YandexSkill\Services\AliceSkillService;

class YandexSkillController extends Controller
{
    private AliceSkillService $dialog;

    private ?string $message_id;

    private ?string $session_id;

    private ?string $skill_id;

    private ?string $user_id;

    private bool $new;

    private bool $end_session;

    private bool $validUser;

    private bool $isAdmin;

    public function index(Request $requestNo)
    {
        $this->message_id = null;
        $this->session_id = null;
        $this->skill_id = null;
        $this->user_id = null;
        $this->new = false;
        $this->end_session = false;
        $this->validUser  = false;
        $this->isAdmin = false;

        $content = file_get_contents('php://input');
        $this->saveToLog($content);
        $request = json_decode($content);
        $session = $request->session;

        $this->message_id = $session->message_id;
        $this->session_id = $session->session_id;
        $this->skill_id = $session->skill_id;
        $this->user_id = $session->user_id;
        $this->new = $session->new;

        $this->process($request->request->nlu->tokens);

        $result = [
            'response' =>
                [
                    'text'        => $this->dialog->text,
                    'tts'         => $this->dialog->text,
                    'end_session' => $this->end_session,
                ],
            'session' =>
                [
                    'session_id'  => $this->session_id,
                    'message_id'  => $this->message_id,
                    'user_id'     => $this->user_id,
                ],
            'version' => '1.0',
        ];

        return response()->json($result);
    }

    private function process(?array $request_json): void
    {
        $this->dialog = new AliceSkillService($request_json);
        $this->dialog->route();
    }

    private function saveToLog($content)
    {
        Log::channel(['alice', 'stdout'])->info($content);
    }
}
