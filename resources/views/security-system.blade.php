@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h1>Охранная система</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h2>Стадия - разработка контроллера</h2>
            </div>
        </div>
    </div>
@endsection
@section('third_party_scripts')
    <script src="{{ asset('js/sensor.js') }}?{{ time() }}"></script>
    <script src="{{ asset('js/relay.js') }}?{{ time() }}"></script>
@endsection
