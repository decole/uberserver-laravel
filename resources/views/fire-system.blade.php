@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h1>Пожарная система</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h2>Стадия - разработка контроллера</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="mb-2"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 5,
                        'title' => 'Низа - температура',
                        'topic' => 'underflor/temperature',
                        'payload' => '12.86'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 8,
                        'title' => 'Кухня - температура',
                        'topic' => 'home/kitchen/temperature',
                        'payload' => '23.00'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 9,
                        'title' => 'Зал - температура',
                        'topic' => 'home/hall/temperature',
                        'payload' => '25.04'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 10,
                        'title' => 'Пристройка - температура',
                        'topic' => 'margulis/temperature',
                        'payload' => 'xx'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 1,
                        'title' => 'Лампа в пристройке',
                        'topic' => 'margulis/lamp01',
                        'check_topic' => 'margulis/check/lamp01',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
@section('third_party_scripts')
    <script src="{{ asset('js/sensor.js') }}?{{ time() }}"></script>
    <script src="{{ asset('js/relay.js') }}?{{ time() }}"></script>
@endsection
