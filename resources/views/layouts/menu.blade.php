<!-- need to remove -->
<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'home') === 0) ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Главная</p>
    </a>
</li>
<li class="nav-item">
<a href="{{ route('main-watering') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'main-watering') === 0) ? 'active' : '' }}">
    <i class="nav-icon fas fa-tint"></i>
    <p>Полив</p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('main-fire-system') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'main-fire-system') === 0) ? 'active' : '' }}">
    <i class="nav-icon fab fa-free-code-camp fa-circle"></i>
    <p>Пожарная система</p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('main-security-system') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'main-security-system') === 0) ? 'active' : '' }}">
    <i class="nav-icon fas fa-user-lock"></i>
    <p>Охранная система</p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('main-all-data') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'main-all-data') === 0) ? 'active' : '' }}">
    <i class="nav-icon fas fa-folder-open"></i>
    <p>Все данные</p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('main-margulis') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'main-margulis') === 0) ? 'active' : '' }}">
    <i class="nav-icon fas fa-border-style"></i>
    <p>Пристройка</p>
</a>
</li>
<li class="nav-item">
<a href="{{ route('main-greenhouse') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'main-greenhouse') === 0) ? 'active' : '' }}">
    <i class="nav-icon fas fa-store-alt"></i>
    <p>Теплица</p>
</a>
</li>
<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-tools"></i>
        <p>
            Настройки
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview" style="display: none;">
        {{--
        @if (Module::find('Mqtt')->isStatus(true))
            <li class="nav-item">
                <a href="pages/layout/top-nav.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Mqtt settings</p>
                </a>
            </li>
        @endif
        --}}
        @if (Module::find('Sensor')->isStatus(true))
            <li class="nav-item">
                <a href="{{ route('sensor.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'sensor.') === 0) ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Настройки Сенсоров</p>
                </a>
            </li>
        @endif
        @if (Module::find('Relay')->isStatus(true))
            <li class="nav-item">
                <a href="{{ route('relay.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'relay.') === 0) ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Настройки Реле</p>
                </a>
            </li>
        @endif
        @if (Module::find('AutoWatering')->isStatus(true))
            <li class="nav-item">
                <a href="{{ route('auto_watering.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'auto_watering.') === 0) ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Настройки Полива</p>
                </a>
            </li>
        @endif
    </ul>
</li>
