@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h1>Все данные</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="mb-2"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 1,
                        'title' => 'Низа - температура',
                        'topic' => 'underflor/temperature',
                        'payload' => '12.86'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 2,
                        'title' => 'Кухня - температура',
                        'topic' => 'home/kitchen/temperature',
                        'payload' => '23.00'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 3,
                        'title' => 'Зал - температура',
                        'topic' => 'home/hall/temperature',
                        'payload' => '25.04'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 4,
                        'title' => 'Холодная прихожка - температура',
                        'topic' => 'holl/temperature',
                        'payload' => '25.04'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 5,
                        'title' => 'Холодная прихожка - влажность',
                        'topic' => 'holl/humidity',
                        'payload' => '25.04'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 6,
                        'title' => 'Низа - температура',
                        'topic' => 'underflor/temperature',
                        'payload' => '25.04'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 7,
                        'title' => 'Пристройка - температура',
                        'topic' => 'margulis/temperature',
                        'payload' => 'xx'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 8,
                        'title' => 'Пристройка - влажность',
                        'topic' => 'margulis/humidity',
                        'payload' => 'xx'
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('sensor::component.sensor', [
                        'id' => 9,
                        'title' => 'Теплица - температура',
                        'topic' => 'greenhouse/temperature',
                        'payload' => 'xx'
                    ])
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 1,
                        'title' => 'Лампа в пристройке',
                        'topic' => 'margulis/lamp01',
                        'check_topic' => 'margulis/check/lamp01',
                        'command_on' => 'on',
                        'command_off' => 'off',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 2,
                        'title' => 'Дом Реле 01',
                        'topic' => 'home/ralay01',
                        'check_topic' => 'home/check/ralay01',
                        'command_on' => 'on',
                        'command_off' => 'off',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 3,
                        'title' => 'Главный клапан',
                        'topic' => 'water/major',
                        'check_topic' => 'water/check/major',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 5,
                        'title' => 'wr01',
                        'topic' => 'water/relay1',
                        'check_topic' => 'water/check/relay1',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 6,
                        'title' => 'wr02',
                        'topic' => 'water/relay2',
                        'check_topic' => 'water/check/relay2',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 7,
                        'title' => 'wr03',
                        'topic' => 'water/relay3',
                        'check_topic' => 'water/check/relay3',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 8,
                        'title' => 'wr01',
                        'topic' => 'water/relay4',
                        'check_topic' => 'water/check/relay4',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 9,
                        'title' => 'wr05',
                        'topic' => 'water/relay5',
                        'check_topic' => 'water/check/relay5',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 10,
                        'title' => 'wr06',
                        'topic' => 'water/relay6',
                        'check_topic' => 'water/check/relay6',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @include('relay::component.relay', [
                        'id' => 11,
                        'title' => 'wr07',
                        'topic' => 'water/relay7',
                        'check_topic' => 'water/check/relay7',
                        'command_on' => '1',
                        'command_off' => '0',
                        'check_command_on' => '1',
                        'check_command_off' => '0',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
@section('third_party_scripts')
    <script src="{{ asset('js/sensor.js') }}?{{ time() }}"></script>
    <script src="{{ asset('js/relay.js') }}?{{ time() }}"></script>
@endsection
