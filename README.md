# What is this

Pet project by my smart home infrastructure.

### First up and build 

`docker-compose up -d --build`

### Upping project 

`make up`

### Seeding sensor topics

`make up`

`make into app`

in container: `php artisan module:seed Sensor`

## UNIT-TESTS

Для тестов нужно создать базу данных и указать ее имя как ключ в .env  `DB_DATABASE_FOR_TESTS`

по дефолту надо создать `uberserver_smarthome_test` - работаю над автоматическим созданием базы

`php artisan migrate --database=testing`

`php artisan test`


----------

# Миграция с Yii2 на Laravel

https://wiki.yaboard.com/w/%D0%90%D0%BB%D0%B8%D1%81%D0%B0_%D0%B8_Home_Assistant
https://dialogs.yandex.ru/developer/skills/
https://yandex.ru/dev/dialogs/smart-home/doc/concepts/platform-protocol.html

 - [x] Перенести без изменений сервисы, чтобы они хотя бы не падали
 - [x] Перенастроить настройки в яндексе ЯУД, Я Скилы, чтобы сервисы хотя бы отвечали
 - [ ] Сделать рефакторинг Я Скилов
 - [ ] Сделать рефакторинг Яндекс Умный Дом

# Миграция на laravel 9
 - [x] Переехать на php 8.1
 - [x] Переехать на модуль mqtt-client поддерживаемый php 8.1
 - [ ] Переделать проект под php8 - с нотациями и т.д.
 - [ ] Настроить docker-composeюньд 

# Провести аудит безопасности
 - [ ] Отключить регистрацию
 - [ ] Сделать логирование под ELK - https://habr.com/ru/post/456676/
 - [ ] Перенести логирование и профилирование на NewRelic One
 - [ ] Прикрутить аналитику c Grafana и Prometeus - https://russianblogs.com/article/3108311032/
